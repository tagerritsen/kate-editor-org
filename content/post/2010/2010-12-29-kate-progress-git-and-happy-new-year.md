---
title: Kate progress, Git and happy new year ;)
author: Christoph Cullmann

date: 2010-12-29T18:04:01+00:00
url: /2010/12/29/kate-progress-git-and-happy-new-year/
categories:
  - Common
  - Developers
  - KDE
tags:
  - planet

---
As Dominik already points out in his [blog][1], Kate has made nice progress for KDE 4.6.

Some of the latest fixes, like for some nasty [search bug][2] didn&#8217;t make it in RC1, btw., I was too lame with Git -> SVN syncs.  
Anyway, I have now synced and backported to KDE 4.6 branch all pending fixes.

For the next KDE SC release 4.7 (here you go, I wrote SC :)), I hope this syncing will no longer be necessary.  
At least Kate app + part + KWrite should then be only in the kate.git.  
I can live with ktexteditor remaining in kdelibs, if removing that and still keeping BC and SC would be too much work. But part and app are at most runtime dependencies anyway.

Last but not least, I wish you all a good start into a happy new year ;)

 [1]: /2010/12/29/kate-in-kde-4-6/
 [2]: https://bugs.kde.org/show_bug.cgi?id=248305