---
title: The Team
author: Christoph Cullmann
date: 2010-07-09T09:03:57+00:00
---

These are the people who are or have been working on creating Kate, KWrite and KatePart. If you are not mentioned and feel you should be — or is mentioned and feel you should not be, [contact us][1].

<div style="margin-bottom: 30px;">
  <div style="width: 150px; float: left;">
    <img title="Kate the Woodpecker" src="/images/kate-source-original.svg" alt="Kate the Woodpecker" width="100" />
  </div>

  <div style="margin-left: 150px;">
    <strong>Kate the Woodpecker</strong>
  </div>

  <div style="margin-left: 150px;">
    Kate the Woodpecker started to be the Kate mascot in 2014.
    She was designed by Tyson Tan and provides a recognizable face to the Kate project.
    In 2020 an icon for the Kate application based on this was introduced.
  </div>
</div>

<div style="margin-bottom: 30px;">
  <div style="width: 150px; float: left;">
    <img title="Christoph Cullmann" src="/wp-content/uploads/2019/04/christoph.png" alt="Christoph Cullmann" width="113" height="113" />
  </div>

  <div style="margin-left: 150px;">
    <strong>Christoph Cullmann</strong>
  </div>

  <div style="margin-left: 150px;">
    Contact: <a href="mailto:cullmann@kde.org">cullmann@kde.org</a> (Jabber: cullmann@jabber.org)<br /> Website: <a href="http://cullmann.io">http://cullmann.io</a>
  </div>

  <div style="margin-left: 150px;">
    Christoph started the application that later became Kate back in 2000, and have stayed with it as a core developer and active maintainer ever since. He has contributed major parts of the code, and is the brain behind the excellent named sessions feature found in Kate 2.5.
  </div>
</div>

<div style="margin-bottom: 30px;">
  <div style="width: 150px; float: left;">
    <img title="Dominik Haumann" src="/wp-content/uploads/2010/07/haumann.jpg" alt="Dominik Haumann" width="113" height="150" />
  </div>

  <div style="margin-left: 150px;">
    <strong>Dominik Haumann</strong>
  </div>

  <div style="margin-left: 150px;">
    Contact: <a href="mailto:dhaumann@kde.org">dhaumann@kde.org</a><a href="mailto:cullmann@kde.org"><br /> </a>Website: <a href="http://www.kate-editor.org">http://www.kate-editor.org</a>
  </div>

  <div style="margin-left: 150px;">
    Dominik joined the Kate Project back in early 2004. Since then he constantly contributes e.g. by maintaining the syntax highlighting files, reviewing patches and developing core features such as scripting support or the line modification system in Kate Part.
  </div>
</div>

<div style="margin-bottom: 30px;">
  <div style="width: 150px; float: left;">
    <img title="Joseph Wenninger" src="/wp-content/uploads/2010/07/jowenn1-127x150.jpg" alt="Joseph Wenninger" width="113" height="141" />
  </div>

  <div style="margin-left: 150px;">
    <strong>Joseph Wenninger<br /> </strong>Contact: <a href="mailto:jowenn@kde.org">jowenn@kde.org</a><br /> Website: <a href="http://www.jowenn.at/blog/">http://www.jowenn.at</a>
  </div>

  <div style="margin-left: 150px;">
    Jowenn has been with Kate development since the early days, and is the developer behind Kate’s excellent syntax highlighting engine, the code folding system, the insertion template system and much more.
  </div>
</div>

<div style="margin-bottom: 30px;">
  <div style="width: 150px; float: left;">
    <img title="Milian Wolff" src="/wp-content/uploads/2010/07/me.jpg" alt="" width="113" height="141" />
  </div>

  <div style="margin-left: 150px;">
    <strong>Milian Wolff</strong>
  </div>

  <div style="margin-left: 150px;">
    Contact: <a href="mailto:mail@milianw.de">mail@milianw.de<br /> </a>Website: <a href="http://milianw.de/">http://milianw.de</a>
  </div>

  <div style="margin-left: 150px;">
    Milian joined the Kate developers in 2009. Since then, he actively contributes in many areas such as maintaining syntax highlighting files, fixing indentation scripts and writing unit tests. He also likes to increase performance and fix general bugs he encounters while using KatePart in KDevelop.
  </div>
</div>

<div style="margin-bottom: 30px;">
  <div style="width: 150px; float: left;">
    <img title="Sven Brauch" src="/wp-content/uploads/2010/07/sven.png" alt="" width="113" height="113" />
  </div>

  <div style="margin-left: 150px;">
    <strong>Sven Brauch</strong>
  </div>

  <div style="margin-left: 150px;">
    Contact: <a href="mailto:svenbrauch@gmail.com">svenbrauch@gmail.com<br /> </a>Website: <a href="http://scummos.blogspot.de">http://scummos.blogspot.de</a>
  </div>

  <div style="margin-left: 150px;">
    Sven is working on various areas of the Kate Part, such as text snippets, code completion, and collaborative text editing.
  </div>
</div>

<div style="margin-bottom: 30px;">
  <div style="width: 150px; float: left;">
    <img title="Miquel Sabaté Solà" src="/wp-content/uploads/2010/07/miquel.jpg" alt="" width="113" height="113" />
  </div>

  <div style="margin-left: 150px;">
    <strong>Miquel Sabaté Solà</strong>
  </div>

  <div style="margin-left: 150px;">
    Contact: <a href="mailto:mikisabate@gmail.com">mikisabate@gmail.com<br /> </a>Website: <a href="http://www.mssola.com">http://www.mssola.com</a>
  </div>

  <div style="margin-left: 150px;">
    Miquel is working tirelessly to make Kate Part&#8217;s vi input mode more and more powerful.
  </div>
</div>

<div style="margin-bottom: 30px;">
  <div style="margin-left: 150px;">
    <strong>Michal Humpula<br /> </strong>Contact: <a href="mailto:michal.humpula@hudrydum.cz">michal.humpula@hudrydum.cz</a>
  </div>

  <div style="margin-left: 150px;">
    Michal is one of the core developers of the vi input mode and did most of the work porting Kate to KDE Frameworks 5.
  </div>
</div>

<div style="margin-bottom: 30px;">
  <div style="width: 150px; float: left;">
    <img title="Kåre Särs" src="/wp-content/uploads/2016/03/Kåre.jpeg" alt="" width="113" height="140" />
  </div>

  <div style="margin-left: 150px;">
    <strong>Kåre Särs<br /> </strong>Contact: <a href="mailto:kare.sars@iki.fi">kare.sars@iki.fi</a><br /> Website: <a href="http://www.kate-editor.org">http://www.kate-editor.org</a>
  </div>

  <div style="margin-left: 150px;">
    Kåre joined the Kate project back in 2008 and since then adds a lot of very useful features through plugins. Famous examples include: CTags, Build plugin, GDB Debugger plugin, Search & Replace in Files.
  </div>
</div>

<div style="margin-bottom: 30px;">
  <div style="margin-left: 150px;">
    <strong>T.C. Hollingsworth<br /> </strong>Contact: <a href="mailto:tchollingsworth@gmail.com">tchollingsworth@gmail.com</a><br /> Website: <a href="http://www.kate-editor.org">http://www.kate-editor.org</a>
  </div>

  <div style="margin-left: 150px;">
    T.C. Hollingsworth maintains the Kate and KWrite documentation since 2011. He keeps track of all changes in Kate development, so that the <a href="http://docs.kde.org/stable/en/kdebase-runtime/kate/index.html">online documentation</a> is always up-to-date.
  </div>
</div>

<div style="margin-bottom: 30px;">
  <div style="width: 150px; float: left;">
    <img title="Alexander Neundorf" src="/wp-content/uploads/2010/07/alex.jpg" alt="" width="113" height="170" />
  </div>

  <div style="margin-left: 150px;">
    <strong>Alexander Neundorf<br /> </strong>Contact: <a href="mailto:neundorf@kde.org">neundorf@kde.org</a><br /> Website: <a href="http://www.kate-editor.org">http://www.kate-editor.org</a>
  </div>

  <div style="margin-left: 150px;">
    Alexander Neundorf maintains the Kate project generator of CMake and contributes to the search, build and project plugins.
  </div>
</div>

<div style="margin-bottom: 30px;">
  <div style="margin-left: 150px;">
    <strong>Shaheed R Haque<br /> </strong>Contact: <a href="mailto:srhaque@theiet.org">srhaque@theiet.org</a><br /> Website: <a href="http://www.kate-editor.org">http://www.kate-editor.org</a>
  </div>

  <div style="margin-left: 150px;">
    Shaheed R Haque has been hacking on KDE projects on-and-off since 2000, including KOffice, KDE libs, Kate (Python plugin support, and some plugins), KDEPIM, multi-language support with a Bengali focus (kxkb, Viki virtual keyboard.) as well as bits and pieces elsewhere in the Open Source world.
  </div>
</div>

<div style="margin-bottom: 30px;">
  <div style="width: 150px; float: left;">
    <img title="Gerald Senarclens de Grancy" src="/wp-content/uploads/2010/07/20080824_180x180.jpeg" alt="Gerald Senarclens de Grancy" width="113" height="113" />
  </div>

  <div style="margin-left: 150px;">
    <strong>Gerald Senarclens de Grancy<br /> </strong>Contact: <a href="mailto:oss@senarclens.eu">oss@senarclens.eu</a><br /> Website: <a href="http://find-santa.eu">http://find-santa.eu</a>
  </div>

  <div style="margin-left: 150px;">
    Gerald uses Kate, Kile and KDevelop on a daily basis. He mainly does QA and scripting to further improve the Kate editor component.
  </div>
</div>

<div style="margin-bottom: 30px;">
  <div style="width: 150px; float: left;">
    <img title="Pablo Martín" src="/wp-content/uploads/2010/07/pmartin.jpg" alt="Pablo Martín" width="113" height="113" />
  </div>

  <div style="margin-left: 150px;">
    <strong>Pablo Martín<br /> </strong>Contact: <a href="mailto:goinnn@gmail.com">goinnn@gmail.com</a><br /> Website: <a href="https://github.com/goinnn">https://github.com/goinnn</a>
  </div>

  <div style="margin-left: 150px;">
    Pablo is a Django developer and uses Kate to code. He joined the Kate Project in 2013, but since 2009 he has been developing plugins for Kate. In 2011, he created a repository on github to develop these plugins and share them, now he codes them in the official repository.
  </div>
</div>

<div style="margin-bottom: 30px;">
  <div style="width: 150px; float: left;">
    <img title="Erlend Hamberg" src="/wp-content/uploads/2010/07/digep0160.jpg" alt="" width="113" height="170" />
  </div>

  <div style="margin-left: 150px;">
    <strong>Erlend Hamberg</strong>
  </div>

  <div style="margin-left: 150px;">
    Contact: <a href="mailto:ehamberg@gmail.com">ehamberg@gmail.com<br /> </a>Website: <a href="http://hamberg.no/erlend/">http://hamberg.no/erlend</a>
  </div>

  <div style="margin-left: 150px;">
    Erlend joined the Kate Project in 2008. Since then, his main focus is on the vi input mode for Kate which make a vim-like, modal editing mode available for Kate.
  </div>
</div>

<div style="margin-bottom: 30px;">
  <div style="margin-left: 150px;">
    <strong>Bernhard Beschow<br /> </strong>Contact: <a href="mailto:bbeschow@cs.tu-berlin.de">bbeschow@cs.tu-berlin.de</a><br /> Website: <a href="http://www.kate-editor.org">http://www.kate-editor.org</a>
  </div>

  <div style="margin-left: 150px;">
    Bernhard joined the Kate Project in 2008 and is doing a great job in refactoring code in Kate to make it very easy to understand. For instance, he is maintaining the excellent search & replace feature as well as the undo/redo system.
  </div>
</div>

<div style="margin-bottom: 30px;">
  <div style="width: 150px; float: left;">
    <img title="Anders Lund" src="/wp-content/uploads/2010/07/anders-138x150.jpg" alt="Anders Lund" width="113" height="123" />
  </div>

  <div style="margin-left: 150px;">
    <strong>Anders Lund</strong>
  </div>

  <div style="margin-left: 150px;">
    Contact: <a href="mailto:anders@alweb.dk">anders@alweb.dk</a><br /> Website: <a href="http://www.alweb.dk/">http://www.alweb.dk/</a>
  </div>

  <div style="margin-left: 150px;">
    Anders has been active in Kate development since the start back in 2000, and have contributed among others the view splitting mechanism (Kate), the External Tools feature (Kate), the document list color shading (Kate) and printing functionality (KatePart).
  </div>
</div>

  * Hamish Rodda
  * Michael Bartl
  * Jochen Wilhelmy

 [1]: mailto:kwrite-devel@kde.org
