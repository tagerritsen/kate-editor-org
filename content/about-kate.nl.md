---
title: Mogelijkheden

author: Tristan Gerritsen

datum: June 6th 2020 7:19 PM
---

## Features

![Schermafdruk die venster van Kate toont met splitsingsfunctie en
terminalplug-in](/images/kate-window.png)

+ Bekijk en bewerk meerdere documenten tegelijk door het scherm horizontaal en/of verticaal te splitsen
+ Veel plug-ins: [Ingebouwde terminal](https://konsole.kde.org), SQL-plug-in, bouwplug-in, GDB-plug-in en meer
+ Sla je project, openstaande tabbladen en map op met sessies.

## Algemene mogelijkheden

![Schermafdruk die de functie zoeken en vervangen in Kate
toont](/images/kate-search-replace.png)

+ Ondersteuning van codering (Unicode en veel andere)
+ Ondersteuning van Bi-directionele tekstweergave
+ Ondersteuning van verschillende regeleinden (Windows, UNIX, Mac), met automatische detectie
+ Netwerktransparantie (bestanden op afstand openen)
+ Breid Kate uit met het maken van scripts.

## Geavanceerde bewerkingsfuncties

![Schermafdruk van rand van Kate met regelnummer en bladwijzer](/images/kate-border.png)

+ Bladwijzers, breakpoints en meer
+ Markering in scrollbar
+ Indicatoren van wijziging in regels
+ Regelnummers
+ In/uitvouwen van codeblokken

## Syntaxisaccentuering

![Schermafdruk van functies voor syntaxisaccentuering van
Kate](/images/kate-syntax.png)

+ Ondersteuning voor accentuering van meer dan 300 programmeertalen
+ Overeen laten komen van haakjes
+ Slimme spellingcontrole
+ Accentuering van geselecteerde woorden

## Programmeermogelijkheden

![Schermafdruk van functies voor programmering van Kate](/images/kate-programming.png)

+ Automatisch inspringen via scripts
+ Slimme verandering van commentaar
+ Automatisch aanvullen met tips voor argumenten
+ Vi invoermodus
+ Modus rechthoekige selectie van blokken

## Zoeken en vervangen

![Schermafdruk met de incrementele zoekfunctie van
Kate](/images/kate-search.png)

+ Incrementeel zoeken, ook bekend als &#8220;zoeken terwijl u typt&#8221;
+ Ondersteuning voor zoeken & vervangen in meerdere regels
+ Ondersteuning voor reguliere expressies
+ Zoeken & vervangen van tekst in meerdere geopende bestanden op de schijf

## Reservekopieën en herstellen

![Schermafdruk met herstel na een crash van Kate](/images/kate-crash.png)

+ Reservekopieën bij opslaan
+ Gegevens herstellen na een crash
+ Maak je wijzigingen ongedaan of herstel ze
