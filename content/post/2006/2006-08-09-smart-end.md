---
title: Smart End
author: Dominik Haumann

date: 2006-08-09T10:33:00+00:00
url: /2006/08/09/smart-end/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2006/08/smart-end.html
categories:
  - Common

---
Since KDE 3.5.4 Kate Part supports &#8220;[smart end][1]&#8220;. Right now it behaves like this when pressing &#8220;end&#8221;:

  1. go to last non-space character
  2. go to the last character (i.e. hit end twice)

I&#8217;m writing this blog to get feedback about what&#8217;s the right/expected behaviour. Is it as described above, or should we first go to the very last character, and to the last non-space only when hitting then end-key twice?

Feedback along with why you prefer solution A or B is welcome!

 [1]: http://bugs.kde.org/show_bug.cgi?id=78258