---
title: Funcionalidades

author: Christoph Cullmann

date: 2010-07-09T08:40:19+00:00
---

## Funcionalidades de la aplicación

![Captura de pantalla que muestra la funcionalidad de división de pantalla
de Kate y el complemento de terminal](/images/kate-window.png)

+ Ver y editar varios documentos a la vez, permitiendo dividir la ventana
horizontal y verticalmente + Gran cantidad de complementos: [terminal
integrado](https://konsole.kde.org), complemento SQL, complemento de
compilación, complemento GDB, sustituir en archivos, y más + Interfaz
multidocumento (MDI) + Permite usar sesiones

## Funcionalidades generales

![Captura de pantalla que muestra la funcionalidad de buscar y sustituir de
Kate](/images/kate-search-replace.png)

+ Permite codificación de caracteres (Unicode y muchos más) + Renderización
de texto bidireccional + Permite el uso de diversos finales de línea
(Windows, Unix, Mac), incluida su detección automática + Transparencia de
red (apertura de archivos remotos) + Se puede extender usando scripts

## Funciones de edición avanzada

![Captura de pantalla del borde de Kate con números de líneas y
marcadores](/images/kate-border.png)

+ Sistema de marcadores (también permite usar puntos de interrupción, etc.)
+ Marcas en la barra de desplazamiento + Indicadores de modificación de
línea + Números de líneas + Plegado de código

## Resaltado de sintaxis

![Captura de pantalla que muestra las funcionalidades de resaltado de
sintaxis de Kate](/images/kate-syntax.png)

+ Resaltado de sintaxis de unos 300 lenguajes + Emparejamiento de llaves +
Comprobación ortográfica inteligente al vuelo + Resaltado de las palabras
seleccionadas

## Funcionalidades de programación

![Captura de pantalla de las funcionalidades de programación de
Kate](/images/kate-programming.png)

+ Sangrado automático mediante scripts + Manejo de creación y eliminación de
comentarios inteligente + Terminación de palabras automática con sugerencias
de argumentos + Modo de entrada Vi + Modo de selección de bloque rectangular

## Buscar y sustituir

![Captura de pantalla que muestra la función de búsqueda incremental de
Kate](/images/kate-search.png)

+ Búsqueda incremental, también conocida como «buscar al escribir» + Permite
usar búsqueda y sustitución multilínea + Permite el uso de expresiones
regulares + Buscar y sustituir en múltiples archivos abiertos o en archivos
en el disco

## Crear y restaurar copias de seguridad

![Captura de pantalla de la función de recuperación de Kate después de un
cuelgue de la aplicación](/images/kate-crash.png)

+ Copias de seguridad al guardar + Archivos de intercambio para la
recuperación de datos tras un cuelgue del sistema + Sistema para deshacer y
rehacer
