---
title: GSoC 2011 – Kate Code Folding – (Try Kate’s new code folding)
author: Adrian Lungu

date: 2011-07-24T17:20:32+00:00
url: /2011/07/24/gsoc-2011-–-kate-code-folding-–-try-kates-new-code-folding/
categories:
  - Developers
  - KDE
  - Users

---
Hi everyone!

Kate&#8217;s new code folding was merged with the [master branch][1]. :)  
You can try it by yourself. If you do find any bugs, please send a bug report on [KDE&#8217;s bug tracker][2] or leave a comment here.  
I will use a test unit to solve those bugs, so I will need a full backtrace and some info to help me replicate it.  
Please notice that there are no new features added yet &#8211; just the basic code folding.

I&#8217;m waiting for your feedback!

 [1]: /get-it/
 [2]: https://bugs.kde.org/