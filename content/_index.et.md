---
layout: index # Don't translated this string
---

Kate on [KDE](ttps://kde.org) mitme dokumendi redigeerimise võimalusega
redaktor juba versioonist 2.2 peale. Kuna see on [KDE
rakendus](https://kde.org/applications), pakub Kate võrgu läbipaistvust ning
ka lõimitust KDE kõige suurepärasemate võimalustega. Sellega saab otse
Konqueroris näha HTML-koodi, muuta seadistusfaile, kirjutada valmis uusi
rakendusi või täita muid tekstiga seotud ülesandeid. Kõigeks selleks läheb
tarvis ainult üht Kate isendit. [Uuri lähemalt ...](/about/)

![Kate ekraanipilt mitme dokumendi ja
terminaliemulaatoriga](/images/kate-window.png)

<div class="text-center">
    <a class="btn btn-success" href="/get-it"><i class="fa fa-download"></i>&nbsp;Hangi see</a>
</div>
