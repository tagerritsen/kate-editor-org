---
title: 'Kate/KWrite on Mac – More Icons & More Plugins'
author: Christoph Cullmann

date: 2015-10-24T15:52:00+00:00
url: /2015/10/24/katekwrite-on-mac-more-icons-more-plugins/
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
After a bit more work, toolbars (and other places ;=) have icons, too. More Kate plugins do work, like the nice project plugin I use the whole day, see:[<img class="aligncenter size-medium wp-image-3701" src="/wp-content/uploads/2015/10/Bildschirmfoto-2015-10-24-um-17.43.00-300x211.png" alt="Kate with Project Plugin on Mac" width="300" height="211" srcset="/wp-content/uploads/2015/10/Bildschirmfoto-2015-10-24-um-17.43.00-300x211.png 300w, /wp-content/uploads/2015/10/Bildschirmfoto-2015-10-24-um-17.43.00-1024x721.png 1024w" sizes="(max-width: 300px) 100vw, 300px" />][1]

Updated .dmg files can be found at (alpha quality, only tested on Mac OS 10.10):

ftp://kate-editor.org/cullmann/kate-20151024.dmg

ftp://kate-editor.org/cullmann/kwrite-20151024.dmg

Help to get more stuff working and fix the remaining crashs is highly appreciated. A script how to build that all can be found at:

https://quickgit.kde.org/?p=kate.git&a=blob&f=mac.txt

You can use a plain Qt and current KDE Frameworks & Kate master to get that running. Guess the next goal would be to get KIO working without patching Qt, lets see how much work that is.

P.S. With the guide in mac.txt you should be able to try out how to port other KDE based applications, too, and get a application bundle, as most stuff should be available now, for the average application. How to use some bundled Breeze icon set can be found here, just call code like that after QApplication is constructed:

https://quickgit.kde.org/?p=kate.git&a=blob&f=icons.h

Where to put the Breeze resource see mac.txt (its actually toplevel in the <app>.app folder, not in Resources like told by Qt docs).

 [1]: /wp-content/uploads/2015/10/Bildschirmfoto-2015-10-24-um-17.43.00.png