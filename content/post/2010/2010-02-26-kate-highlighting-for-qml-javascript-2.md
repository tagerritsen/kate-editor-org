---
title: Kate Highlighting for QML, JavaScript
author: Milian Wolff

date: 2010-02-26T14:45:21+00:00
excerpt: |
  Hey everyone!
  
  
  
  
  
  I&#8217;ve started my internship at KDAB this week, it&#8217;s great fun so far! Though I spent most of my time this week on Bertjans KDevelop plugin, I couldn&#8217;t resist on a bit of Kate hacking:
  
  steveire is experimenting with ...
url: /2010/02/26/kate-highlighting-for-qml-javascript-2/
syndication_source:
  - 'Milian Wolff - kate'
syndication_source_uri:
  - http://milianw.de/taxonomy/term/170/0
syndication_source_id:
  - http://milianw.de/tag/kate/feed
"rss:comments":
  - 'http://milianw.de/blog/kate-highlighting-for-qml-javascript#comments'
"wfw:commentRSS":
  - http://milianw.de/crss/node/123
syndication_feed:
  - http://milianw.de/tag/kate/feed
syndication_feed_id:
  - "8"
syndication_permalink:
  - http://milianw.de/blog/kate-highlighting-for-qml-javascript
syndication_item_hash:
  - be3066f46ec52c4b1548f03e8cc98c0c
  - 8f8637bbda5824c7dc5e13f7b126e9d2
  - eea66de08cf8e26ff1b5d42b5043ecee
categories:
  - KDE

---
Hey everyone!

<div style="float:right">
  <a href="http://www.flickr.com/photos/milianw/4389235505" title="qmlhlQML Highlighting in Kate"><img src="http://farm5.staticflickr.com/4027/4389235505_144e6d3def_m.jpg" alt="qmlhlQML Highlighting in Kate" title="qmlhlQML Highlighting in Kate"  class=" flickr-photo-img" height="240" width="197" /></a>
</div>

I&#8217;ve started my internship at [KDAB][1] this week, it&#8217;s great fun so far! Though I spent most of my time this week on [Bertjans KDevelop plugin][2], I couldn&#8217;t resist on a bit of Kate hacking:

[steveire is experimenting with QML][3] so I couldn&#8217;t stop but notice that there is no highlighting for it in Kate. Well, there **was** none ;-) Now you get pretty colors, rejoice!

Note: Since QML is basically JSON with some added sugar, I reused the existing JavaScript highlighter and improved it. Hence you get imrpoved JSON and member highlighting in plain<span class="geshifilter"><code class="text geshifilter-text">.js</code></span> as well. Enjoy!

 [1]: http://www.kdab.com/
 [2]: http://bertjan.broeksemaatjes.nl/node/51
 [3]: http://steveire.wordpress.com/2010/02/19/qml-with-akonadi-or-how-to-use-your-existing-model-view-design-with-qml/