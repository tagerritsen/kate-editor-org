---
layout: index # Don't translated this string
---

Kate is dé tekstbewerker voor allerlei documenten en is onderdeel van
[KDE](https://kde.org) sinds versie 2.2. Omdat het bij de [KDE toepassingen]
(https://kde.org/applications) hoort, wordt Kate geleverd met
netwerktransparantie en integratie met de geweldige mogelijkheden van KDE.
Kies Kate voor het bekijken van HTML broncode uit Konqueror, het bewerken
van configuratiebestanden, maken van nieuwe toepassingen en nog veel meer.
[Leer meer...](/about/)

![Schermafdruk van Kate dat meerdere documenten toont](/images/kate-window.png)

<div class="text-center">
    <a class="btn btn-success" href="/get-it"><i class="fa fa-download"></i>&nbsp;Downloaden</a>
</div>
