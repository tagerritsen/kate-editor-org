---
title: Kate plugin updates part 3
author: Kåre Särs

date: 2011-06-28T05:03:34+00:00
url: /2011/06/28/kate-plugin-updates-part-3/
categories:
  - Developers
  - Users
tags:
  - planet

---
Well I&#8217;m not sure this really qualifies as an update, because it is actually a new plugin. And the plugin I&#8217;m talking about is the Search plugin. This is going to be mostly a series of screen-shots.  
The first shot contains the &#8220;new&#8221; feature differentiating itself from the other one. You can now select to search in the currently open files. This is especially good if you are working on files on a remote server.

<p style="text-align: center">
  <a href="/wp-content/uploads/2011/06/search_in_open.png"><img src="/wp-content/uploads/2011/06/search_in_open.png" alt="" width="572" height="278" /></a>
</p>

&nbsp;

The &#8220;Find in files&#8221; plugin opens new searches in new toolviews while this one has a button for adding new tabs if you want to save the old search.

&nbsp;

<p style="text-align: center">
  <a href="/wp-content/uploads/2011/06/search_with_tabs.png"><img class="alignnone size-full wp-image-990" src="/wp-content/uploads/2011/06/search_with_tabs.png" alt="" width="585" height="273" srcset="/wp-content/uploads/2011/06/search_with_tabs.png 585w, /wp-content/uploads/2011/06/search_with_tabs-300x140.png 300w" sizes="(max-width: 585px) 100vw, 585px" /></a>
</p>

&nbsp;

Of course you can also search in a folder as before :) When you select to search in a folder the folder searching specific options are displayed. Those options can also be toggled with the rightmost tool-button.

&nbsp;

<p style="text-align: center">
  <a href="/wp-content/uploads/2011/06/search_in_files.png"><img class="alignnone size-full wp-image-991 aligncenter" src="/wp-content/uploads/2011/06/search_in_files.png" alt="" width="584" height="230" srcset="/wp-content/uploads/2011/06/search_in_files.png 584w, /wp-content/uploads/2011/06/search_in_files-300x118.png 300w" sizes="(max-width: 584px) 100vw, 584px" /></a>
</p>

&nbsp;

And the final screen-shot is to show that it is possible to use regular expressions.

<p style="text-align: center">
  <a href="/wp-content/uploads/2011/06/search_options.png"><img class="alignnone size-full wp-image-992" src="/wp-content/uploads/2011/06/search_options.png" alt="" width="626" height="220" srcset="/wp-content/uploads/2011/06/search_options.png 626w, /wp-content/uploads/2011/06/search_options-300x105.png 300w" sizes="(max-width: 626px) 100vw, 626px" /></a>
</p>

&nbsp;

The &#8220;Replace&#8221; feature is not yet implemented, but who knows ;) Ideas for a UI are welcome.  
And that was it for this time. I hope you like the updates :)