---
title: Extending Kate with Scripts
author: Dominik Haumann

date: 2009-10-29T18:41:00+00:00
url: /2009/10/29/extending-kate-with-scripts/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2009/10/extending-kate-with-scripts.html
pw_single_layout:
  - "1"
categories:
  - Developers
  - Users

---
As mentioned in one of my <a title="News from the Holy Kate Land" href="/2009/09/18/news-from-the-holy-kate-land/" target="_self">last blogs</a>, there has been quite some work for JavaScript scripting support in trunk recently. So what will Kate 3.4 (KDE 4.4) will bring? This is explained in detail in the sections:

  * Indentation Scripting
  * Command Line Scripting
  * Some Remarks

The scripting allows to extend Kate with lots of little helper functions. All users can add scripts as they wish. The documentation here is copied from the <a title="Kate Handbook" href="http://docs.kde.org/stable/en/kde-baseapps/kate/advanced-editing-tools-scripting.html" target="_blank" rel="noopener">official Kate handbook</a>. So instead of using this blog as reference, please use the handbook later for an up-to-date version. To script something useful, you&#8217;ll need to know the scripting API. All the available functions are documented in the section <a title="Kate Scripting API" href="https://docs.kde.org/stable5/en/applications/katepart/dev-scripting.html" target="_blank" rel="noopener">Scripting API in the handbook</a> as well. Enough said, here we go:

<div class="titlepage">
  <div>
    <div>
      <h2 class="title" style="clear: both;">
        <a name="advanced-editing-tools-scripting"></a>
      </h2>
    </div>
  </div>
</div>

<div class="titlepage">
  <div>
    <div>
      <h2 class="title" style="clear: both;">
        <a name="advanced-editing-tools-scripting"></a>Extending <span class="application">Kate</span> with Scripts
      </h2>
    </div>
  </div>
</div>

Since <span class="application">Kate</span> 3.4 in KDE 4.4 the <span class="application">Kate</span> editor component is easily extensible by writing scripts. The scripting language is ECMAScript (widely known as JavaScript). <span class="application">Kate</span> supports two kinds of scripts: indentation and command line scripts.

<div class="sect2" lang="en">
  <div class="titlepage">
    <div>
      <div>
        <h3 class="title">
          <a name="advanced-editing-tools-scripting-indentation"></a>Indentation Scripts
        </h3>
      </div>
    </div>
  </div>
  
  <p>
    Indentation scripts &#8211; also referred as indenters &#8211; automatically indent the source code while typing text. As example, after hitting the return-key code the indentation level often increases.
  </p>
  
  <p>
    The following sections describe step by step how to create the skeleton for a simple indenter. As first step, create a new <code class="filename">*.js</code> file called e.g. <code class="filename">javascript.js</code> in the local home folder <code class="filename">$KDEHOME/share/apps/katepart/script</code>.
  </p>
  
  <div class="sect3" lang="en">
    <div class="titlepage">
      <div>
        <div>
          <h4 class="title">
            <a name="advanced-editing-tools-scripting-indentation-header"></a>The Indentation Script Header
          </h4>
        </div>
      </div>
    </div>
    
    <p>
      The header of the file <code class="filename">javascript.js</code> is embedded in a comment and is of the following form
    </p>
    
    <pre>/* kate-script
 * name: JavaScript
 * author: Example Name
 * license: BSD
 * revision: 1
 * kate-version: 3.4
 * type: indentation
 * required-syntax-style: javascript
 * indent-languages: javascript
 * priority: 0
 *
 * A line without colon ':' stops header parsing. That is, you can add optional
 * text here such as a detailed license.
 */</pre>
    
    <p>
      Each entry is explained in detail now:
    </p>
    
    <div class="itemizedlist">
      <ul type="disc">
        <li>
          <code class="literal">kate-script</code> [required]: This text string has to appear in the first line of the <code class="filename">*.js</code> file, otherwise <span class="application">Kate</span> skips the script.
        </li>
        <li>
          <code class="literal">name</code> [required]: This is the indenter name that appears in the menu <span class="guiitem"><span class="guimenu">Tools</span></span>-><span class="guiitem"><span class="guimenuitem">Indentation</span></span> and in the configuration dialog.
        </li>
        <li>
          <code class="literal">author</code> [optional]: The author&#8217;s name and contact information.
        </li>
        <li>
          <code class="literal">license</code> [optional]: Short form of the license, such as BSD or LGPLv3.
        </li>
        <li>
          <code class="literal">revision</code> [required]: The revision of the script. This number should be increased whenever the script is modified.
        </li>
        <li>
          <code class="literal">kate-version</code> [required]: Minimal required <span class="application">Kate</span> version.
        </li>
        <li>
          <code class="literal">type</code> [required]: The type must be “<span class="quote"><code class="literal">indentation</code></span>”, otherwise <span class="application">Kate</span> skips this script.
        </li>
        <li>
          <code class="literal">required-syntax-style</code> [optional]: Comma separated list of required syntax highlighting styles. This is important for indenters that rely on specific highlight information in the document. If a required syntax style is specified, the indenter is available only when the appropriate highlighter is active. This prevents “<span class="quote">undefined behavior</span>” caused by using the indenter without the expected highlighting schema. For instance, the Ruby indenter makes use of this in the files <code class="filename">ruby.js</code> and <code class="filename">ruby.xml</code>.
        </li>
        <li>
          <code class="literal">indent-languages</code> [optional]: Comma separated list of syntax styles the indenter can indent correctly, e.g.: c++, java.
        </li>
        <li>
          <code class="literal">priority</code> [optional]: If several indenters are suited for a certain highlighted file, the priority decides which indenter is chosen as default indenter.
        </li>
      </ul>
    </div>
    
    <p>
      <span class="application">Kate</span> reads all pairs of the form “<span class="quote"><span class="replaceable"><em class="replaceable"><code>key</code></em></span>:<span class="replaceable"><em class="replaceable"><code>value</code></em></span></span>” until it cannot fine a colon anymore. This implies that the header can contain arbitrary text such as a license as shown in the example.
    </p>
  </div>
  
  <div class="sect3" lang="en">
    <div class="titlepage">
      <div>
        <div>
          <h4 class="title">
            <a name="advanced-editing-tools-scripting-indentation-body"></a>The Indenter Source Code
          </h4>
        </div>
      </div>
    </div>
    
    <p>
      Having specified the header this section explains how the indentation scripting itself works. The basic skeleton of the body looks like this:
    </p>
    
    <pre>triggerCharacters = "{}/:;";
function indent(line, indentWidth, ch)
{
  // called for each newline (ch == '\n') and all characters specified in
  // the global variable triggerCharacters. When calling Tools -&gt; Align
  // the variable ch is empty, i.e. ch == ''.
  //
  // see also: Scripting API
  return -2;
}</pre>
    
    <p>
      The function <code class="function">indent()</code> has three parameters:
    </p>
    
    <div class="itemizedlist">
      <ul type="disc">
        <li>
          <code class="literal">line</code>: the line that has to be indented
        </li>
        <li>
          <code class="literal">indentWidth</code>: the indentation width in amount of spaces
        </li>
        <li>
          <code class="literal">ch</code>: either a newline character (<code class="literal">ch == '\n'</code>), the trigger character specified in <code class="literal">triggerCharacters</code> or empty if the user invoked the action <span class="guiitem"><span class="guimenu">Tools</span></span>-><span class="guiitem"><span class="guimenuitem">Align</span></span>.
        </li>
      </ul>
    </div>
    
    <p>
      The return value of the <code class="function">indent()</code> function specifies how the line will be indented. If the return value is a simple integer number, it is interpreted as follows:
    </p>
    
    <div class="itemizedlist">
      <ul type="disc">
        <li>
          return value <code class="literal">-2</code>: do nothing
        </li>
        <li>
          return value <code class="literal">-1</code>: keep indentation (searches for previous non-blank line)
        </li>
        <li>
          return value <code class="literal"> 0</code>: numbers >= 0 specify the indentation depth in spaces
        </li>
      </ul>
    </div>
    
    <p>
      Alternatively, an array of two elements can be returned:
    </p>
    
    <div class="itemizedlist">
      <ul type="disc">
        <li>
          <code class="literal">return [ indent, align ];</code>
        </li>
      </ul>
    </div>
    
    <p>
      In this case, the first element is the indentation depth like above with the same meaning of the special values. However, the second element is an absolute value representing a column for “<span class="quote">alignment</span>”. If this value is higher than the indent value, the difference represents a number of spaces to be added after the indentation of the first parameter. Otherwise, the second number is ignored. Using tabs and spaces for indentation is often referred to as “<span class="quote">mixed mode</span>”.
    </p>
    
    <p>
      Consider the following example: Assume using tabs to indent, and tab width is set to 4. Here, <tab> represents a tab and &#8216;.&#8217; a space:
    </p>
    
    <pre>1: &lt;tab&gt;&lt;tab&gt;foobar("hello",
2: &lt;tab&gt;&lt;tab&gt;......."world");</pre>
    
    <p>
      When indenting line 2, the <code class="function">indent()</code> function returns [8, 15]. As result, two tabs are inserted to indent to column 8, and 7 spaces are added to align the second parameter under the first, so that it stays aligned if the file is viewed with a different tab width.
    </p>
    
    <p>
      A default KDE installation ships <span class="application">Kate</span> with several indenters. The corresponding JavaScript source code can be found in <code class="filename">$KDRDIR/share/apps/katepart/script</code>.
    </p>
    
    <p>
      Developing an indenter requires to reload the scripts to see whether the changes behave appropriately. Instead of restarting the application, simply switch to the command line and invoke the command <span class="command"><strong class="command">reload-scripts</strong></span>.
    </p>
    
    <p>
      If you develop useful scripts please consider contributing to the <span class="application">Kate</span> Project by <a href="mailto:kwrite-devel@kde.org" target="_top">contacting the mailing list</a>.
    </p>
  </div>
</div>

<div class="sect2" lang="en">
  <div class="titlepage">
    <div>
      <div>
        <h3 class="title">
          <a name="advanced-editing-tools-scripting-command-line"></a>Command Line Scripts
        </h3>
      </div>
    </div>
  </div>
  
  <p>
    As it is hard to satisfy everyone&#8217;s needs, <span class="application">Kate</span> supports little helper tools for quick text manipulation through the <a title="The Editor Component Command Line" href="http://docs.kde.org/development/en/kdesdk/kate/advanced-editing-tools-commandline.html">built-in command line</a>. For instance, the command <span class="command"><strong class="command">sort</strong></span> is implemented as script. This section explains how to create <code class="filename">*.js</code> files to extend <span class="application">Kate</span> with arbitrary helper scripts.
  </p>
  
  <p>
    Command line scripts are located in the save folder as indentation scripts. So as first step, create a new <code class="filename">*.js</code> file called <code class="filename">myutils.js</code> in the local home folder <code class="filename">$KDEHOME/share/apps/katepart/script</code>.
  </p>
  
  <div class="sect3" lang="en">
    <div class="titlepage">
      <div>
        <div>
          <h4 class="title">
            <a name="advanced-editing-tools-scripting-command-line-header"></a>The Command Line Script Header
          </h4>
        </div>
      </div>
    </div>
    
    <p>
      The header of each command line script is embedded in a comment and is of the following form
    </p>
    
    <pre>/* kate-script
 * author: Example Name
 * license: BSD
 * revision: 1
 * kate-version: 3.4
 * type: commands
 * functions: sort, format-paragraph
 *
 * A line without colon ':' stops header parsing. That is, you can add optional
 * text here such as a detailed license.
 */</pre>
    
    <p>
      Each entry is explained in detail now:
    </p>
    
    <div class="itemizedlist">
      <ul type="disc">
        <li>
          <code class="literal">kate-script</code> [required]: This text string has to appear in the first line of the <code class="filename">*.js</code> file, otherwise <span class="application">Kate</span> skips the script.
        </li>
        <li>
          <code class="literal">author</code> [optional]: The author&#8217;s name and contact information.
        </li>
        <li>
          <code class="literal">license</code> [optional]: Short form of the license, such as BSD or LGPLv3.
        </li>
        <li>
          <code class="literal">revision</code> [required]: The revision of the script. This number should be increased whenever the script is modified.
        </li>
        <li>
          <code class="literal">kate-version</code> [required]: Minimal required <span class="application">Kate</span> version.
        </li>
        <li>
          <code class="literal">type</code> [required]: The type must be &#8216;commands&#8217;, otherwise <span class="application">Kate</span> skips this script.
        </li>
        <li>
          <code class="literal">functions</code> [required]: Comma separated list of commands in the script.
        </li>
      </ul>
    </div>
    
    <p>
      <span class="application">Kate</span> reads all pairs of the form “<span class="quote"><span class="replaceable"><em class="replaceable"><code>key</code></em></span>:<span class="replaceable"><em class="replaceable"><code>value</code></em></span></span>” until it cannot fine a colon anymore. This implies that the header can contain arbitrary text such as a license as shown in the example. The value of the key functions is a comma separated list of command line commands. This means a single script contains an arbitrary amount of command line commands. Each function is available through <span class="application">Kate</span>&#8216;s <a title="The Editor Component Command Line" href="http://docs.kde.org/development/en/kdesdk/kate/advanced-editing-tools-commandline.html">built-in command line</a>.
    </p>
  </div>
  
  <div class="sect3" lang="en">
    <div class="titlepage">
      <div>
        <div>
          <h4 class="title">
            <a name="advanced-editing-tools-scripting-command-line-body"></a>The Script Source Code
          </h4>
        </div>
      </div>
    </div>
    
    <p>
      All functions specified in the header have to be implemented in the script. For instance, the script file from the example above needs to implement the two functions <span class="command"><strong class="command">sort</strong></span> and <span class="command"><strong class="command">format-paragraph</strong></span>. All functions have the following syntax:
    </p>
    
    <pre class="programlisting">function (arg1, arg2, ...) {
  // ... implementation, see also: Scripting API
}</pre>
    
    <p>
      Arguments in the command line are passed to the function as <span class="parameter"><em class="parameter"><code>arg1</code></em></span>, <span class="parameter"><em class="parameter"><code>arg2</code></em></span>, etc. In order to provide documentation for each command, simply implement the &#8216;<code class="function">help</code>&#8216; function as follows:
    </p>
    
    <pre class="programlisting">function help(cmd)
{
  if (cmd == "sort") {
    return "Sort the selected text.";
  } else if (cmd == "...") {
    // ...
  }
}</pre>
    
    <p>
      Executing <span class="command"><strong class="command">help sort</strong></span> in the command line then calls this help function with the argument <span class="parameter"><em class="parameter"><code>cmd</code></em></span> set to the given command, i.e. <span class="parameter"><em class="parameter"><code>cmd == "sort"</code></em></span>. <span class="application">Kate</span> then presents the returned text as documentation to the user.
    </p>
    
    <p>
      Developing a command line script requires to reload the scripts to see whether the changes behave appropriately. Instead of restarting the application, simply switch to the command line and invoke the command <span class="command"><strong class="command">reload-scripts</strong></span>.
    </p>
    
    <p>
      If you develop useful scripts please consider contributing to the <span class="application">Kate</span> Project by <a href="mailto:kwrite-devel@kde.org" target="_top">contacting the mailing list</a>.
    </p>
    
    <h2 class="title" style="clear: both;">
      Final Remarks
    </h2>
    
    <p>
      The command line scripting can be accessed for all KTextEditor users through the <a href="http://api.kde.org/4.x-api/kdelibs-apidocs/interfaces/ktexteditor/html/classKTextEditor_1_1CommandInterface.html">KTextEditor::CommandInterface</a>. That is, you can query a specific command and <a href="http://api.kde.org/4.x-api/kdelibs-apidocs/interfaces/ktexteditor/html/classKTextEditor_1_1Command.html#afcf59107ca87d860fc7a17e507df02d2">execute it</a> with arbitrary parameters (The parameter cmd contains the command itself including all arguments. Example: cmd = &#8220;goto 65&#8221;).
    </p>
    
    <p>
      Kate&#8217;s command line itself is actually a quite powerful tool. It&#8217;s a little bit sad that it&#8217;s rather unknown. If you want to know more, just invoke &#8220;View -> Swith to command line&#8221; (shortcut: F7) and start typing text. More details are in the <a title="Kate Command Line" href="http://docs.kde.org/stable/en/kde-baseapps/kate/advanced-editing-tools-commandline.html" target="_blank" rel="noopener">Kate handbook</a> as well.
    </p>
    
    <p>
      The Kate scripting API can be found <a title="Kate Scripting API" href="http://docs.kde.org/stable/en/kde-baseapps/kate/advanced-editing-tools-scripting.html#advanced-editing-tools-scripting-api" target="_blank" rel="noopener">here</a>.
    </p>
  </div>
</div>