---
title: Scripting in Kate
author: Dominik Haumann

date: 2012-01-21T11:13:41+00:00
url: /2012/01/21/scripting-in-kate/
pw_single_layout:
  - "1"
categories:
  - Developers
  - Users
tags:
  - planet

---
Since several releases, <a title="Scripting in Kate" href="https://docs.kde.org/trunk5/en/applications/katepart/dev-scripting.html" target="_blank">Kate Part has scripting support</a> through javascript. So far, it seems it is not much used by users. Still, I stumbled over two extensions:

  * <a title="Jump to previous / next paragraph" href="http://kucrut.org/move-cursor-to-next-prev-paragraph-in-kate/" target="_blank">jump to previous / next paragraph</a>
  * <a title="base 64 encoding" href="http://kde-apps.org/content/show.php/Kate+Base64+support?content=138046" target="_blank">base64 encoding</a>

If you have more user defined scripts, it would be nice if you let us know! For KDE5, we plan to extend this, so applications like Kile or KDevelop can reuse Kate&#8217;s internal code.

On another note, here is <a title="KDE 4.8 release" href="http://cristalinux.blogspot.com/2012/01/kde-48-upcoming-features.html" target="_blank">a nice blog</a> about the upcoming KDE 4.8 release :-)