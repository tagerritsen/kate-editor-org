---
title: Características

author: Christoph Cullmann

date: 2010-07-09T08:40:19+00:00
---

## Funcionalidades da Aplicação

![Imagem que mostra a funcionalidade de janelas divididas do Kate e o
'plugin' do terminal](/images/kate-window.png)

+ Visualização e edição de vários documentos ao mesmo tempo, dividindo a
janela na horizontal e na vertical + Diversos 'plugins': [Terminal
incorporado](https://konsole.kde.org), 'plugin' de SQL, 'plugin' de
compilação, 'plugin' do GDB, Substituição nos Ficheiros, entre outros

## Funcionalidades Gerais

![Imagem que mostra a funcionalidade de pesquisa e substituição do
Kate](/images/kate-search-replace.png)

+ Suporte para diferentes codificações (Unicode e muitos outros) + Suporte
para o desenho de texto bidireccional + Suporte para diferentes fins de
linha (Windows, Unix, Mac), incluindo a detecção automática + Transparência
na rede (abertura de ficheiros remotos)  + Extensível através de programação

## Funcionalidades Avançadas do Editor

![Imagem do contorno do Kate com o número da linha e os
favoritos](/images/kate-border.png)

+ Sistema de marcação de favoritos (também suportado: pontos de paragem
etc.)  + Marcações na barra de deslocamento + Indicadores de modificação das
linhas + Números de linhas + Dobragem de código

## Realce de Sintaxe

![Imagem das funcionalidades de realce de sintaxe do
Kate](/images/kate-syntax.png)

+ Suporte de realce para mais de 300 linguagens + Correspondência de
parêntesis + Verificação ortográfica inteligente em tempo-real + Realce de
palavras seleccionadas

## Funcionalidades de Programação

![Imagem das funcionalidades de programação do
Kate](/images/kate-programming.png)

+ Indentação automática programável + Tratamento inteligente de comentários
e remoção dos mesmos + Completação automática com sugestões de argumentos +
Modo de selecção em bloco rectangular

## Pesquisa & Substituição

![Imagem da funcionalidade de pesquisa incremental do
Kate](/images/kate-search.png)

+ Pesquisa incremental, também conhecida por &#8220;pesquisar à medida que
escreve&#8221; + Suporte para pesquisa & substituição multi-linhas + Suporte
para expressões regulares + Pesquisa & substituição em vários ficheiros
abertos ou nos ficheiros do disco

## Salvaguarda e Reposição

![Imagem do Kate a recuperar de um estoiro](/images/kate-crash.png)

+ Cópias de segurança ao gravar + Ficheiros temporários para recuperar os
dados em caso de estoiro do sistema
