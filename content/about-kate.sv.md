---
title: Funktioner

author: Christoph Cullmann

date: 2010-07-09T08:40:19+00:00
---

## Programfunktioner

![Skärmbild som visar Kates fönsterdelningsfunktion och
terminalinsticksprogram](/images/kate-window.png)

+ Visa och redigera flera dokument samtidigt genom att dela fönstret
horisontellt och vertikalt + Många insticksprogram: [inbäddad
terminal](https://konsole.kde.org), SQL-insticksprogram,
bygginsticksprogram, GDB-insticksprogram, Ersätt i filer, med mera +
Gränssnitt för flera dokument (MDI) + Sessionsstöd

## Allmänna funktioner

![Skärmbild som visar Kates söknings- och
ersättningsfunktion](/images/kate-search-replace.png)

+ Kodningsstöd (Unicode och många andra) + Stöd för bidirektionell
textåtergivning + Radslutstöd (Windows, Unix Mac), inklusive automatisk
detektering + Nätverkstransparens (öppna fjärrfiler) + Utökningsbar via
skript

## Avancerade redigeringsfunktioner

![Skärmbild av Kates kant med radnummer och
bokmärke](/images/kate-border.png)

+ Bokmärkessystem (stöder också: brytpunkter etc.) + Rullningsmarkörer +
Radändringsindikering + Radnummer + Kodvikning

## Syntaxfärgläggning

![Skärmbild av Kates syntaxfärgläggningsfunktion](/images/kate-syntax.png)

+ Färgläggningsstöd för över 300 språk + Parentesmatchning + Smart
stavningskontroll i farten + Färgläggning av markerade ord

## Programmeringsfunktioner

![Skärmbild av Kates programmingsfunktioner](/images/kate-programming.png)

+ Skriptbar automatisk indentering + Smart hantering av kommentartillägg och
borttagning + Automatisk komplettering med argumenttips + Vi-inmatningsläge
+ Rektangulärt markeringsläge

## Sök och ersätt

![Skärmbild av Kates inkrementella sökfunktion](/images/kate-search.png)

+ Inkrementell sökning, också känd som &#8220;sök medan du skriver&#8221; +
Stöd för flerraders sök och ersätt + Stöd för reguljära uttryck + Sök och
ersätt i flera öppna filer eller filer på disk

## Säkerhetskopiera och återställ

![Skärmbild av Kates funktion för
kraschåterställning](/images/kate-crash.png)

+ Säkerhetskopiera vid spara + Växlingsfiler för att återställa data vid
systemkrascher + Ångra eller gör om system
