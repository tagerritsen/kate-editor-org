---
title: 'Kate Meeting: Day 0'
author: Dominik Haumann

date: 2008-04-11T16:20:00+00:00
url: /2008/04/11/kate-meeting-day-0/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2008/04/kate-meeting-day-0.html
categories:
  - Events

---
Finally it all begins: Anders and Joseph arrived at basysKom and we&#8217;ve started to discuss some things we want to do for KDE 4.1. Later, we are going to meet with the rest of the attendees in a restaurant to get to know each other. The official start of the meeting is tomorrow morning. If you are interested in contributing to Kate, just join #kate on irc.kde.org. I&#8217;m looking forward to the next two days :)