---
title: Kate polishing
author: Milian Wolff

date: 2010-01-21T00:09:47+00:00
excerpt: 'Phew, I just finished some last-minute backports to the KDE 4.3.5 branch. Lets hope the bug fixes I and pletourn did are as good as they look. Expect a much more stable Kate for 4.3.5 &amp; 4.4! We managed to fix two bugs which are potentially the caus...'
url: /2010/01/21/kate-polishing/
syndication_source:
  - 'Milian Wolff - kate'
syndication_source_uri:
  - http://milianw.de/taxonomy/term/170/0
syndication_source_id:
  - http://milianw.de/tag/kate/feed
"rss:comments":
  - 'http://milianw.de/blog/kate-polishing#comments'
"wfw:commentRSS":
  - http://milianw.de/crss/node/117
syndication_feed:
  - http://milianw.de/tag/kate/feed
syndication_feed_id:
  - "8"
syndication_permalink:
  - http://milianw.de/blog/kate-polishing
syndication_item_hash:
  - 1a93144507171b037a4f7b6843d6f645
  - 617416791838fba469ec3a5fee79100e
categories:
  - KDE

---
Phew, I just finished some last-minute backports to the KDE 4.3.5 branch. Lets hope the bug fixes I and pletourn did are as good as they look. Expect a much more stable Kate for 4.3.5 & 4.4! We managed to fix two bugs which are potentially the cause for dozens of bug reports, all seemingly random. Lets see whether our fixes hold up to our hopes!

Other than that: You should look forward to Kate scripting (with JavaScript) in 4.4. It&#8217;s dead simple but actually useful. In the <span class="geshifilter"><code class="text geshifilter-text">utils.js</code></span> file we ship with Kate there are now the following tools (all operate on the selection or - if none exists - on the whole document):

  * sort - simple sorting
  * natsort - natural sorting
  * uniq - filter duplicates
  * trim - remove leading & trailing whitespace
  * ltrim - remove leading whitespace
  * rtrim - remove trailing whitespace

Do you have more ideas for such simple helper functions?