---
title: Syntax Highlightings
author: Christoph Cullmann
date: 2019-08-24T12:12:12+00:00
---

<!-- This page is auto-generated by update-syntax.pl -->

Kate's highlighting powered by [KSyntaxHighlighting](https://api.kde.org/frameworks/syntax-highlighting/html/) supports the following 319 languages.

### 3D
* [G-Code](/syntax/data/syntax/gcode.xml)
* [GLSL](/syntax/data/syntax/glsl.xml)
* [OpenSCAD](/syntax/data/syntax/openscad.xml)
* [PLY](/syntax/data/syntax/ply.xml)
* [POV-Ray](/syntax/data/syntax/povray.xml)
* [RenderMan RIB](/syntax/data/syntax/rib.xml)
* [STL](/syntax/data/syntax/stl.xml)
* [VRML](/syntax/data/syntax/vrml.xml)
* [Wavefront OBJ](/syntax/data/syntax/wavefront-obj.xml)

### Assembler
* [AVR Assembler](/syntax/data/syntax/asm-avr.xml)
* [Asm6502](/syntax/data/syntax/asm6502.xml)
* [Common Intermediate Language (CIL)](/syntax/data/syntax/cil.xml)
* [GNU Assembler](/syntax/data/syntax/gnuassembler.xml)
* [Intel x86 (FASM)](/syntax/data/syntax/fasm.xml)
* [Intel x86 (NASM)](/syntax/data/syntax/nasm.xml)
* [MIPS Assembler](/syntax/data/syntax/mips.xml)
* [Motorola 68k (VASM/Devpac)](/syntax/data/syntax/asm-m68k.xml)
* [Motorola DSP56k](/syntax/data/syntax/asm-dsp56k.xml)
* [PicAsm](/syntax/data/syntax/picsrc.xml)

### Configuration
* [.desktop](/syntax/data/syntax/desktop.xml)
* [Adblock Plus](/syntax/data/syntax/adblock.xml)
* [Apache Configuration](/syntax/data/syntax/apache.xml)
* [Cisco](/syntax/data/syntax/cisco.xml)
* [Doxyfile](/syntax/data/syntax/doxyfile.xml)
* [Git Ignore](/syntax/data/syntax/git-ignore.xml)
* [Gitolite](/syntax/data/syntax/gitolite.xml)
* [INI Files](/syntax/data/syntax/ini.xml)
* [Kconfig](/syntax/data/syntax/kconfig.xml)
* [Nagios](/syntax/data/syntax/nagios.xml)
* [QDoc Configuration](/syntax/data/syntax/qdocconf.xml)
* [TOML](/syntax/data/syntax/toml.xml)
* [Varnish 4 Configuration Language](/syntax/data/syntax/varnish4.xml)
* [Varnish 4 Test Case language](/syntax/data/syntax/varnishtest4.xml)
* [Varnish Configuration Language](/syntax/data/syntax/varnish.xml)
* [Varnish Test Case language](/syntax/data/syntax/varnishtest.xml)
* [WINE Config](/syntax/data/syntax/winehq.xml)
* [fstab](/syntax/data/syntax/fstab.xml)
* [kdesrc-buildrc](/syntax/data/syntax/kdesrc-buildrc.xml)
* [mergetag text](/syntax/data/syntax/mergetagtext.xml)
* [x.org Configuration](/syntax/data/syntax/xorg.xml)

### Database
* [4GL](/syntax/data/syntax/fgl-4gl.xml)
* [4GL-PER](/syntax/data/syntax/fgl-per.xml)
* [LDIF](/syntax/data/syntax/ldif.xml)
* [SQL](/syntax/data/syntax/sql.xml)
* [SQL (MySQL)](/syntax/data/syntax/sql-mysql.xml)
* [SQL (Oracle)](/syntax/data/syntax/sql-oracle.xml)
* [SQL (PostgreSQL)](/syntax/data/syntax/sql-postgresql.xml)
* [progress](/syntax/data/syntax/progress.xml)

### Hardware
* [AHDL](/syntax/data/syntax/ahdl.xml)
* [Spice](/syntax/data/syntax/spice.xml)
* [SystemC](/syntax/data/syntax/systemc.xml)
* [SystemVerilog](/syntax/data/syntax/systemverilog.xml)
* [VHDL](/syntax/data/syntax/vhdl.xml)
* [Vera](/syntax/data/syntax/vera.xml)
* [Verilog](/syntax/data/syntax/verilog.xml)

### Markup
* [ASN.1](/syntax/data/syntax/asn1.xml)
* [ASP](/syntax/data/syntax/asp.xml)
* [AppArmor Security Profile](/syntax/data/syntax/apparmor.xml)
* [AsciiDoc](/syntax/data/syntax/asciidoc.xml)
* [BibTeX](/syntax/data/syntax/bibtex.xml)
* [CSS](/syntax/data/syntax/css.xml)
* [CartoCSS](/syntax/data/syntax/carto-css.xml)
* [CleanCSS](/syntax/data/syntax/ccss.xml)
* [ColdFusion](/syntax/data/syntax/coldfusion.xml)
* [ConTeXt](/syntax/data/syntax/context.xml)
* [DTD](/syntax/data/syntax/dtd.xml)
* [Django HTML Template](/syntax/data/syntax/djangotemplate.xml)
* [Doxygen](/syntax/data/syntax/doxygen.xml)
* [DoxygenLua](/syntax/data/syntax/doxygenlua.xml)
* [FTL](/syntax/data/syntax/ftl.xml)
* [GNU Gettext](/syntax/data/syntax/gettext.xml)
* [GlossTex](/syntax/data/syntax/glosstex.xml)
* [HTML](/syntax/data/syntax/html.xml)
* [Haml](/syntax/data/syntax/haml.xml)
* [Hamlet](/syntax/data/syntax/hamlet.xml)
* [JSON](/syntax/data/syntax/json.xml)
* [JSP](/syntax/data/syntax/jsp.xml)
* [Javadoc](/syntax/data/syntax/javadoc.xml)
* [Jira](/syntax/data/syntax/jira.xml)
* [LESSCSS](/syntax/data/syntax/less.xml)
* [LaTeX](/syntax/data/syntax/latex.xml)
* [MAB-DB](/syntax/data/syntax/mab.xml)
* [Mako](/syntax/data/syntax/mako.xml)
* [Markdown](/syntax/data/syntax/markdown.xml)
* [MediaWiki](/syntax/data/syntax/mediawiki.xml)
* [Metapost/Metafont](/syntax/data/syntax/metafont.xml)
* [Mustache/Handlebars (HTML)](/syntax/data/syntax/mustache.xml)
* [Pango](/syntax/data/syntax/pango.xml)
* [PostScript](/syntax/data/syntax/postscript.xml)
* [PostScript Printer Description](/syntax/data/syntax/ppd.xml)
* [Pug](/syntax/data/syntax/pug.xml)
* [R Markdown](/syntax/data/syntax/rmarkdown.xml)
* [R documentation](/syntax/data/syntax/rdoc.xml)
* [RELAX NG](/syntax/data/syntax/relaxng.xml)
* [RelaxNG-Compact](/syntax/data/syntax/relaxngcompact.xml)
* [Rich Text Format](/syntax/data/syntax/rtf.xml)
* [Roff](/syntax/data/syntax/roff.xml)
* [Ruby/Rails/RHTML](/syntax/data/syntax/rhtml.xml)
* [SASS](/syntax/data/syntax/sass.xml)
* [SCSS](/syntax/data/syntax/scss.xml)
* [SGML](/syntax/data/syntax/sgml.xml)
* [SiSU](/syntax/data/syntax/sisu.xml)
* [TT2](/syntax/data/syntax/template-toolkit.xml)
* [Texinfo](/syntax/data/syntax/texinfo.xml)
* [Textile](/syntax/data/syntax/textile.xml)
* [Troff Mandoc](/syntax/data/syntax/mandoc.xml)
* [Wesnoth Markup Language](/syntax/data/syntax/wml.xml)
* [XML](/syntax/data/syntax/xml.xml)
* [XML (Debug)](/syntax/data/syntax/xmldebug.xml)
* [XUL](/syntax/data/syntax/xul.xml)
* [YAML](/syntax/data/syntax/yaml.xml)
* [YANG](/syntax/data/syntax/yang.xml)
* [reStructuredText](/syntax/data/syntax/rest.xml)
* [txt2tags](/syntax/data/syntax/txt2tags.xml)
* [vCard, vCalendar, iCalendar](/syntax/data/syntax/vcard.xml)
* [xslt](/syntax/data/syntax/xslt.xml)

### Other
* [ABC](/syntax/data/syntax/abc.xml)
* [CMake](/syntax/data/syntax/cmake.xml)
* [CUE Sheet](/syntax/data/syntax/cue.xml)
* [ChangeLog](/syntax/data/syntax/changelog.xml)
* [Debian Changelog](/syntax/data/syntax/debianchangelog.xml)
* [Debian Control](/syntax/data/syntax/debiancontrol.xml)
* [Diff](/syntax/data/syntax/diff.xml)
* [Dockerfile](/syntax/data/syntax/dockerfile.xml)
* [Email](/syntax/data/syntax/email.xml)
* [GDB](/syntax/data/syntax/gdb.xml)
* [GDB Backtrace](/syntax/data/syntax/gdb-bt.xml)
* [GDB Init](/syntax/data/syntax/gdbinit.xml)
* [Git Rebase](/syntax/data/syntax/git-rebase.xml)
* [GraphQL](/syntax/data/syntax/graphql.xml)
* [Hunspell Affix File](/syntax/data/syntax/hunspell-aff.xml)
* [Hunspell Dictionary File](/syntax/data/syntax/hunspell-dic.xml)
* [Hunspell Thesaurus File](/syntax/data/syntax/hunspell-dat.xml)
* [Hunspell Thesaurus Index File](/syntax/data/syntax/hunspell-idx.xml)
* [InnoSetup](/syntax/data/syntax/innosetup.xml)
* [Intel HEX](/syntax/data/syntax/intelhex.xml)
* [Jam](/syntax/data/syntax/jam.xml)
* [Java Properties](/syntax/data/syntax/java-properties.xml)
* [LilyPond](/syntax/data/syntax/lilypond.xml)
* [Logcat](/syntax/data/syntax/logcat.xml)
* [M3U](/syntax/data/syntax/m3u.xml)
* [MIB](/syntax/data/syntax/mib.xml)
* [Makefile](/syntax/data/syntax/makefile.xml)
* [Meson](/syntax/data/syntax/meson.xml)
* [Music Publisher](/syntax/data/syntax/mup.xml)
* [Ninja](/syntax/data/syntax/ninja.xml)
* [Overpass QL](/syntax/data/syntax/overpassql.xml)
* [PGN](/syntax/data/syntax/pgn.xml)
* [QMake](/syntax/data/syntax/qmake.xml)
* [RPM Spec](/syntax/data/syntax/rpmspec.xml)
* [SELinux File Contexts](/syntax/data/syntax/selinux-fc.xml)
* [SubRip Subtitles](/syntax/data/syntax/subrip-subtitles.xml)
* [Tiger](/syntax/data/syntax/tiger.xml)
* [Valgrind Suppression](/syntax/data/syntax/valgrind-suppression.xml)
* [Wayland Trace](/syntax/data/syntax/wayland-trace.xml)

### Scientific
* [Ansys](/syntax/data/syntax/ansys.xml)
* [B-Method](/syntax/data/syntax/bmethod.xml)
* [FASTQ](/syntax/data/syntax/fastq.xml)
* [GAP](/syntax/data/syntax/gap.xml)
* [GDL](/syntax/data/syntax/gdl.xml)
* [Gnuplot](/syntax/data/syntax/gnuplot.xml)
* [Magma](/syntax/data/syntax/magma.xml)
* [Mathematica](/syntax/data/syntax/mathematica.xml)
* [Matlab](/syntax/data/syntax/matlab.xml)
* [Maxima](/syntax/data/syntax/maxima.xml)
* [Metamath](/syntax/data/syntax/metamath.xml)
* [Octave](/syntax/data/syntax/octave.xml)
* [R Script](/syntax/data/syntax/r.xml)
* [Replicode](/syntax/data/syntax/replicode.xml)
* [Stan](/syntax/data/syntax/stan.xml)
* [Stata](/syntax/data/syntax/stata.xml)
* [TI Basic](/syntax/data/syntax/tibasic.xml)
* [dot](/syntax/data/syntax/dot.xml)
* [scilab](/syntax/data/syntax/sci.xml)
* [yacas](/syntax/data/syntax/yacas.xml)

### Scripts
* [4DOS BatchToMemory](/syntax/data/syntax/4dos.xml)
* [AMPLE](/syntax/data/syntax/ample.xml)
* [AWK](/syntax/data/syntax/awk.xml)
* [AutoHotKey](/syntax/data/syntax/ahk.xml)
* [Bash](/syntax/data/syntax/bash.xml)
* [BrightScript](/syntax/data/syntax/brightscript.xml)
* [CLIST](/syntax/data/syntax/clist.xml)
* [Chicken](/syntax/data/syntax/chicken.xml)
* [CoffeeScript](/syntax/data/syntax/coffee.xml)
* [CubeScript](/syntax/data/syntax/cubescript.xml)
* [Erlang](/syntax/data/syntax/erlang.xml)
* [Euphoria](/syntax/data/syntax/euphoria.xml)
* [Fish](/syntax/data/syntax/fish.xml)
* [GNU Linker Script](/syntax/data/syntax/ld.xml)
* [J](/syntax/data/syntax/j.xml)
* [JCL](/syntax/data/syntax/jcl.xml)
* [JavaScript](/syntax/data/syntax/javascript.xml)
* [JavaScript React (JSX)](/syntax/data/syntax/javascript-react.xml)
* [LSL](/syntax/data/syntax/lsl.xml)
* [Lua](/syntax/data/syntax/lua.xml)
* [MEL](/syntax/data/syntax/mel.xml)
* [MS-DOS Batch](/syntax/data/syntax/dosbat.xml)
* [Mason](/syntax/data/syntax/mason.xml)
* [NSIS](/syntax/data/syntax/nsis.xml)
* [Perl](/syntax/data/syntax/perl.xml)
* [Pig](/syntax/data/syntax/pig.xml)
* [Pike](/syntax/data/syntax/pike.xml)
* [PowerShell](/syntax/data/syntax/powershell.xml)
* [Praat](/syntax/data/syntax/praat.xml)
* [Puppet](/syntax/data/syntax/puppet.xml)
* [Python](/syntax/data/syntax/python.xml)
* [QML](/syntax/data/syntax/qml.xml)
* [Quake Script](/syntax/data/syntax/idconsole.xml)
* [REXX](/syntax/data/syntax/rexx.xml)
* [Raku](/syntax/data/syntax/raku.xml)
* [Ruby](/syntax/data/syntax/ruby.xml)
* [Scheme](/syntax/data/syntax/scheme.xml)
* [Sieve](/syntax/data/syntax/sieve.xml)
* [TaskJuggler](/syntax/data/syntax/taskjuggler.xml)
* [Tcl/Tk](/syntax/data/syntax/tcl.xml)
* [Tcsh](/syntax/data/syntax/tcsh.xml)
* [TypeScript](/syntax/data/syntax/typescript.xml)
* [TypeScript React (TSX)](/syntax/data/syntax/typescript-react.xml)
* [UnrealScript](/syntax/data/syntax/uscript.xml)
* [Velocity](/syntax/data/syntax/velocity.xml)
* [Xonotic Script](/syntax/data/syntax/xonotic-console.xml)
* [Zsh](/syntax/data/syntax/zsh.xml)
* [ferite](/syntax/data/syntax/ferite.xml)
* [k](/syntax/data/syntax/k.xml)
* [q](/syntax/data/syntax/q.xml)
* [sed](/syntax/data/syntax/sed.xml)

### Sources
* [ABAP](/syntax/data/syntax/abap.xml)
* [ANS-Forth94](/syntax/data/syntax/ansforth94.xml)
* [ANSI C89](/syntax/data/syntax/ansic89.xml)
* [ActionScript 2.0](/syntax/data/syntax/actionscript.xml)
* [Ada](/syntax/data/syntax/ada.xml)
* [Agda](/syntax/data/syntax/agda.xml)
* [Bitbake](/syntax/data/syntax/bitbake.xml)
* [Boo](/syntax/data/syntax/boo.xml)
* [C](/syntax/data/syntax/c.xml)
* [C#](/syntax/data/syntax/cs.xml)
* [C++](/syntax/data/syntax/cpp.xml)
* [CGiS](/syntax/data/syntax/cgis.xml)
* [Cg](/syntax/data/syntax/cg.xml)
* [Clipper](/syntax/data/syntax/clipper.xml)
* [Clojure](/syntax/data/syntax/clojure.xml)
* [Common Lisp](/syntax/data/syntax/commonlisp.xml)
* [Component-Pascal](/syntax/data/syntax/component-pascal.xml)
* [Crack](/syntax/data/syntax/crk.xml)
* [Curry](/syntax/data/syntax/curry.xml)
* [D](/syntax/data/syntax/d.xml)
* [E Language](/syntax/data/syntax/e.xml)
* [Eiffel](/syntax/data/syntax/eiffel.xml)
* [Elixir](/syntax/data/syntax/elixir.xml)
* [Elm](/syntax/data/syntax/elm.xml)
* [FSharp](/syntax/data/syntax/fsharp.xml)
* [FlatBuffers](/syntax/data/syntax/flatbuffers.xml)
* [Fortran (Fixed Format)](/syntax/data/syntax/fortran-fixed.xml)
* [Fortran (Free Format)](/syntax/data/syntax/fortran-free.xml)
* [FreeBASIC](/syntax/data/syntax/freebasic.xml)
* [GNU M4](/syntax/data/syntax/m4.xml)
* [Go](/syntax/data/syntax/go.xml)
* [Groovy](/syntax/data/syntax/groovy.xml)
* [Haskell](/syntax/data/syntax/haskell.xml)
* [Haxe](/syntax/data/syntax/haxe.xml)
* [IDL](/syntax/data/syntax/idl.xml)
* [ILERPG](/syntax/data/syntax/ilerpg.xml)
* [ISO C++](/syntax/data/syntax/isocpp.xml)
* [Inform](/syntax/data/syntax/inform.xml)
* [Java](/syntax/data/syntax/java.xml)
* [Julia](/syntax/data/syntax/julia.xml)
* [KBasic](/syntax/data/syntax/kbasic.xml)
* [KDev-PG[-Qt] Grammar](/syntax/data/syntax/grammar.xml)
* [Kotlin](/syntax/data/syntax/kotlin.xml)
* [LPC](/syntax/data/syntax/lpc.xml)
* [Lex/Flex](/syntax/data/syntax/lex.xml)
* [Literate Curry](/syntax/data/syntax/literate-curry.xml)
* [Literate Haskell](/syntax/data/syntax/literate-haskell.xml)
* [Logtalk](/syntax/data/syntax/logtalk.xml)
* [Modelica](/syntax/data/syntax/modelica.xml)
* [Modula-2](/syntax/data/syntax/modula-2.xml)
* [Modula-2 (ISO only)](/syntax/data/syntax/modula-2-iso-only.xml)
* [Modula-2 (PIM only)](/syntax/data/syntax/modula-2-pim-only.xml)
* [Modula-2 (R10 only)](/syntax/data/syntax/modula-2-r10-only.xml)
* [MonoBasic](/syntax/data/syntax/monobasic.xml)
* [Nemerle](/syntax/data/syntax/nemerle.xml)
* [OORS](/syntax/data/syntax/oors.xml)
* [OPAL](/syntax/data/syntax/opal.xml)
* [Objective Caml](/syntax/data/syntax/ocaml.xml)
* [Objective Caml Ocamllex](/syntax/data/syntax/ocamllex.xml)
* [Objective Caml Ocamlyacc](/syntax/data/syntax/ocamlyacc.xml)
* [Objective-C](/syntax/data/syntax/objectivec.xml)
* [Objective-C++](/syntax/data/syntax/objectivecpp.xml)
* [OpenCL](/syntax/data/syntax/opencl.xml)
* [PL/I](/syntax/data/syntax/pli.xml)
* [Pascal](/syntax/data/syntax/pascal.xml)
* [Pony](/syntax/data/syntax/pony.xml)
* [Prolog](/syntax/data/syntax/prolog.xml)
* [Protobuf](/syntax/data/syntax/protobuf.xml)
* [PureBasic](/syntax/data/syntax/purebasic.xml)
* [RSI IDL](/syntax/data/syntax/rsiidl.xml)
* [RapidQ](/syntax/data/syntax/rapidq.xml)
* [RenPy](/syntax/data/syntax/renpy.xml)
* [Rust](/syntax/data/syntax/rust.xml)
* [SELinux CIL Policy](/syntax/data/syntax/selinux-cil.xml)
* [SELinux Policy](/syntax/data/syntax/selinux.xml)
* [SML](/syntax/data/syntax/sml.xml)
* [Sather](/syntax/data/syntax/sather.xml)
* [Scala](/syntax/data/syntax/scala.xml)
* [Smali](/syntax/data/syntax/smali.xml)
* [Solidity](/syntax/data/syntax/solidity.xml)
* [TADS 3](/syntax/data/syntax/tads3.xml)
* [Vala](/syntax/data/syntax/vala.xml)
* [Varnish 3 module spec file](/syntax/data/syntax/varnishcc.xml)
* [Varnish 4 module spec file](/syntax/data/syntax/varnishcc4.xml)
* [Yacc/Bison](/syntax/data/syntax/yacc.xml)
* [Zonnon](/syntax/data/syntax/zonnon.xml)
* [nesC](/syntax/data/syntax/nesc.xml)
* [noweb](/syntax/data/syntax/noweb.xml)
* [xHarbour](/syntax/data/syntax/xharbour.xml)

### Hidden Languages
* [Alerts](/syntax/data/syntax/alert.xml)
* [GCCExtensions](/syntax/data/syntax/gcc.xml)
* [Modelines](/syntax/data/syntax/modelines.xml)
* [PHP/PHP](/syntax/data/syntax/php.xml)
