---
pealkiri: Omadused

autor: Christoph Cullmann

kuupäev: 2010-07-09T08:40:19+00:00
---

## Rakenduse omadused

![Kate ekraanipilt poolitatud akna ja
terminalipluginaga](/images/kate-window.png)

 + Mitme dokumendi korraga vaatamine ja muutmine akent rõht- ja püstsuunas
poolitades + Palju pluginaid: [põimitud terminal](https://.konsole.org),
SQL-i plugin, ehitamisplugin, GDB plugin, failides asendamise plugin ja
palju muud + Mitmedokumendiliides (MDI) + Seansside toetus

## Üldised omadused

![Kate ekraanipilt otsimise ja asendamise
võimalusega](/images/kate-search-replace.png)

 + Kodeeringute toetus (Unicode ja veel palju teisi) + Kahesuunalise teksti
renderdamise toetus + Realõppude toetus (Windows, Unix, Mac), kaasa arvatud
automaatne tuvastamine + Võrgu läbipaistvus (kaugfailide avamine) +
Laiendamine skriptide abil

## Muud redaktori omadused

![Kate piirde ekraanipilt reanumbri ja
järjehoidjaga](/images/kate-border.png)

 + Järjehoidjate süsteem (toetatud on ka katkestuspunktid jms) + Kerimisriba
märgised + Rea muutmise tähised + Reanumbrid + Koodi voltimine

## Süntaksi esiletõstmine

![Kate ekraanipilt süntaksi esiletõstmisega](/images/kate-syntax.png)

 + Esiletõstmise toetus enam kui 300 keeles + Sulgude sobitamine + Nutikas
reaalajas õigekirja kontroll + Valitud sõnade esiletõstmine

## Programmeerimisomadused

![Kate ekraanipilt
programmeerimisvõimalustega](/images/kate-programming.png)

 + Skriptitav automaatne treppimine + Nutikas kommenteerimise ja kommentaari
eemaldamise süsteem + Automaatne lõpetamine ühes argumentide vihjetega +
Ristkülikukujulise plokkvaliku režiim

## Otsimine ja asendamine

![Kate ekraanipilt inkrementotsingu võimalusega](/images/kate-search.png)

 + Inkrementotsing, tuntud ka kui &#8220;otsimine kirjutamise ajal&#8221; +
Mitmel real otsimise ja asendamise toetus + Regulaaravaldiste toetus +
Otsimine ja asendamine mitmes avatud failis või kettal paiknevates failides

## Varundamine ja taastamine

![Kate ekraanipilt krahhijärgse taastamise
võimalusega](/images/kate-crash.png)

 + Varundamine salvestamisel + Saalefailid andmete taastamiseks süsteemi
krahhi järel + Tagasivõtmiste ja uuestitegemiste süsteem
