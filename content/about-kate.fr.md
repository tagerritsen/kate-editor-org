---
title: Fonctionnalités

author: Christoph Cullmann

date: 2010-07-09T08:40:19+00:00
---

## Fonctionnalités de l'application

 ! [Copie d'écran de Kate, affichant la fonctionnalité de partage de
fenêtres et le module externe de terminal] (/images/kate-window.png)

+ Affiche et modifie de multiples documents en même temps, grâce au
découpage horizontal et vertical de la fenêtre + de nombreux modules
externes :[Terminal intégré ](https://konsole.kde.org), module externe
« SQL », module externe de compilation, module externe « GDB », remplacement
dans les fichiers et bien plus + Interface Multi-document (MDI) + prise en
charge de session

## Fonctionnalités générales

 ! [Copie d'écran de Kate, affichant la fonctionnalité de recherche et de
remplacement] (/images/kate-search-replace.png)

+ Prise en charge de l'encodage (Unicode et beaucoup d'autres) + Prise en
charge du rendu de texte bi-directionnel + Prise en charge de la fin de
ligne (Windows, Unix, Mac) y compris l'auto-détection + Transparence réseau
(fichiers distants ouverts) + Langage extensible de scripts

## Fonctionnalités avancées de l'éditeur

 ! [Copie d'écran de la bordure Kate avec les numéros de ligne et les
signets] (/images/kate-crashpng)

+ Système de signets (prend en charge aussi : points d'arrêt, etc.) +
marques de barre de défilement + Numéros de ligne + Pliage de code

## Coloration syntaxique

 ! [Copie d'écran de Kate pour la fonctionnalité de coloration syntaxique]
(/images/kate-syntax.png)

+ Prise en charge du surlignage pour plus de 300 langues + Vérification des
parenthèses + Vérification d'orthographe intelligente à la saisie +
Surlignage des mots sélectionnés

## Fonctionnalités de programmation

 ! [Copie d'écran de Kate pour la fonctionnalité de programmation]
(/images/kate-programming.png)

+ Auto-indentation scriptable + Commentaire intelligent et traitement non
commenté + Auto-complètement avec des indications d'arguments + Mode de
saisie de « vi » + Mode de sélection par bloc rectangulaire

## Chercher et remplacer

 ! [Copie d'écran de Kate pour la fonctionnalité de recherche incrémentale]
(/images/kate-search.png)

+ Recherche incrémentale, aussi connue comme &#8220; trouver comme vous avez
saisi &#8221; + Prise en charge pour la recherche et le remplacement
multi-ligne + Prise en charge des expressions rationnelles + Recherche &
Remplacement dans des fichiers multiples ouverts ou fichiers sur disque

## Sauvegarder et restaurer

 ! [Copie d'écran de Kate pour la fonctionnalité de récupération de données
après plantage] (/images/kate-crash.png)

+ Sauvegarde par enregistrement + Échanger les fichiers pour retrouver les
données après un plantage du système + Annuler / Refaire
