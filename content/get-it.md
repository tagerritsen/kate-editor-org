---
title: Get Kate
author: Christoph Cullmann
date: 2010-07-09T14:40:05+00:00
---

<table style="border-collapse: collapse; border: none;">
<tr style="border: none;">
<td style="border: none;" valign="top">
<img src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" width="100" height="100" />
</td>
<td style="border: none;">
<h3>
Linux & Unices
</h3>
<ul>
  <li>
    Install Kate/KWrite <a title="Distributions Shipping KDE Software" href="http://www.kde.org/download/distributions.php" target="_blank" rel="noopener noreferrer">from your distribution</a>.
  </li>
  <li>
    <a href="/build-it/#linux">Build it</a> from source.
  </li>
</ul>
</td>
</tr>
</table>

<br /><br />

<table style="border-collapse: collapse; border: none;">
<tr style="border: none;">
<td style="border: none;" valign="top">
<img src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" width="100" height="100" />
</td>
<td style="border: none;">
<h3>
Windows
</h3>
<ul>
  <li>
    <a href="https://www.microsoft.com/store/apps/9NWMW7BB59HW" target="_blank" rel="noopener noreferrer">Kate in the Windows Store</a>
  </li>
  <li>
    <a href="https://chocolatey.org/packages/kate" target="_blank" rel="noopener noreferrer">Kate via Chocolatey</a>
  </li>
  <li>
    <a href="https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/" target="_blank" rel="noopener noreferrer">Kate release (64bit) installer</a>
  </li>
  <li>
    <a href="https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/" target="_blank" rel="noopener noreferrer">Kate nightly (64bit) installer</a>
  </li>
  <li>
    <a href="/build-it/#windows">Build it</a> from source.
  </li>
</ul>
</td>
</tr>
</table>

<br /><br />

<table style="border-collapse: collapse; border: none;">
<tr style="border: none;">
<td style="border: none;" valign="top">
<img src="/wp-content/uploads/2010/07/OS_X_El_Capitan_logo.svg_.png" alt="OS_X_El_Capitan_logo.svg" width="100" height="100" />
</td>
<td style="border: none;">
<h3>
macOS
</h3>
<ul>
  <li>
    <a href="https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/">Kate release installer</a>
  </li>
  <li>
    <a href="https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/">Kate nightly installer</a>
  </li>
  <li>
    <a href="/build-it/#mac">Build it</a> from source.
  </li>
</ul>
</td>
</tr>
</table>
