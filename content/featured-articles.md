---
title: Featured Articles
author: Dominik Haumann
date: 2010-07-10T10:50:49+00:00
---

# About the Kate Project

Some information about the scope and aims of this project and the components that are created:

* <a title="About the Kate Project" href="/about/" target="_self">Introduction to the Kate Project</a>
* More Details on <a title="About Kate" href="/about-kate/" target="_self">Kate / KWrite</a> and <a title="About Kate Part" href="/about-katepart/" target="_self">Kate Part</a>
* <a title="Getting Support" href="/support/" target="_self">Getting Support</a>
* <a title="The Kate Team" href="/the-team/" target="_self">People behind the project</a>

# Using Kate and KWrite

Information for the advanced user, how to use scripts, write extensions, ...

* <a title="Writing Syntax Highlighting Files" href="/2005/03/24/writing-a-syntax-highlighting-file/" target="_self">Writing Syntax Highlighting Files</a>
* <a title="Using Kate Modelines" href="/2006/02/09/kate-modelines/" target="_self">Using Kate Modelines (document variables)</a>
* <a title="kateconfig Files" href="/2006/02/09/kateconfig-files/" target="_self">Configuring Kate with a .kateconfig file</a>
* <a title="Extending Kate with Scripts" href="/2009/10/29/extending-kate-with-scripts/" target="_self">Extending Kate with Scripts</a>
* <a title="The Kate Vi Mode" href="/kate-vi-mode/" target="_self">The Kate Vi Mode</a>

# Using Kate Plugins

Information about Kate plugins, which are cool?

* <a title="Kate SQL Plugin" href="/2010/07/29/katesql-a-new-plugin-for-kate/" target="_self">Kate SQL Plugin</a>
* <a title="Kate XML Completion Plugin" href="/2013/10/26/kate-xml-completion-converting-dtd-to-metadtd/">Kate XML Completion Plugin</a>
* <a title="GDB Backtrace Browser Plugin" href="/2008/08/12/kate-fast-backtrace-navigation/" target="_self">GDB Backtrace Browser</a>
* <a title="Projects Plugin" href="/2012/11/02/using-the-projects-plugin-in-kate/" target="_self">Projects Plugin</a>

# Getting Involved

Entry points for people wanting to contribute:

* [Building Kate from Source](/build-it/)
* <a title="Debugging Kate with Qt Creator" href="/2010/06/19/kate-debugging-with-qt-creator/" target="_self">Debugging Kate with Qt Creator</a>
* Writing KTextEditor Plugins
* <a title="Writing Kate Plugins" href="/2004/01/06/writing-a-kate-plugin/" target="_self">Writing Kate Plugins</a>

# Kate Bug Tracker

Where to find our current issues? Help welcome to fix them ;)

* <a title="Weekly Summary for Kate/KWrite" href="http://bugs.kde.org/weekly-bug-summary.cgi">Weekly Summary for Kate/KWrite Bugs</a>
* <a title="Bug Charts for KSyntaxHighlighting" href="https://bugs.kde.org/reports.cgi?product_id=754&#038;datasets=UNCONFIRMED&#038;datasets=CONFIRMED&#038;datasets=ASSIGNED&#038;datasets=REOPENED" target="_blank">Bug Charts for KSyntaxHighlighting</a>
* <a title="Bug Charts for KTextEditor" href="https://bugs.kde.org/reports.cgi?product_id=623&#038;datasets=UNCONFIRMED&#038;datasets=CONFIRMED&#038;datasets=ASSIGNED&#038;datasets=REOPENED" target="_blank">Bug Charts for KTextEditor</a>
* <a title="Bug Charts for Kate and KWrite" href="https://bugs.kde.org/reports.cgi?product_id=39&#038;datasets=UNCONFIRMED&#038;datasets=CONFIRMED&#038;datasets=ASSIGNED&#038;datasets=REOPENED" target="_blank">Bug Charts for Kate/KWrite</a>
* <a title="KSyntaxHighlighting Bug Tracker" href="http://bugs.kde.org/buglist.cgi?product=frameworks-syntax-highlighting&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor">KSyntaxHighlighting Bug Tracker</a>
* <a title="Kate/KWrite Bug Tracker" href="http://bugs.kde.org/buglist.cgi?product=frameworks-ktexteditor&product=kate&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor">Kate and KWrite Bug Tracker</a>
* <a title="Kate/KWrite Wishlist" href="http://bugs.kde.org/buglist.cgi?product=frameworks-ktexteditor&product=kate&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=wishlist">Feature Wishlist for Kate and KWrite</a>
* <a title="Kate's Reviewboard" href="https://git.reviewboard.kde.org/groups/kate/" target="_blank">Submit and Review Patches</a>

# Kate Internals

Concepts of the Kate implementation:

* <a title="Kate Internals: Kate's Text Buffer" href="/2010/03/03/kate-internals-text-buffer/" target="_self">The Text Buffer</a>
* <a title="The Undo/Redo System" href="/2008/11/14/kate-internals-the-undoredo-system/" target="_self">The Undo/Redo System</a>

# More Kate Resources

Build bot and co.:

* <a title="build.kde.org Jenkins for kate.git master" href="https://build.kde.org/job/kate%20master%20kf5-qt5/" target="_blank">build.kde.org Jenkins for kate.git master</a>
* Git Statistics: <a title="KTextEditor" href="https://github.com/KDE/ktexteditor/graphs/contributors" target="_blank">KTextEditor.git</a>, <a title="Kate" href="https://github.com/KDE/kate/graphs/contributors" target="_blank">Kate.git</a>
