---
layout: index # Don't translated this string
---

Kate est un éditeur multi-document, membre de [KDE](https://kde.org) depuis
la mise à jour 2.2. En étant une [KDE
applications](https://kde.org/applications), Kate est fourni avec une
transparence réseau, ainsi qu'une intégration avec les fonctionnalités
remarquables de KDE. Choisissez le pour afficher des sources « HTML »,
écrire de nouvelles applications ou réaliser toutes les tâches de
modifications de texte. Vous avez juste besoin d'un instance de Kate en
exécution. [Plus d'informations...](/about/)

 ! [Copie d'écran de Kate, affichant de multiples documents et l'émulateur
intégré de terminal (/images/kate-window.png)

<div class="text-center">
    <a class="btn btn-success" href="/get-it"><i class="fa fa-download"></i>&nbsp;Adoptez le</a>
</div>
