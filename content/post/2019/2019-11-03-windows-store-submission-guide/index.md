---
title: Windows Store Submission Guide
author: Christoph Cullmann
date: 2019-11-03T14:00:00+02:00
url: /post/2019/2019-11-03-windows-store-submission-guide/
---

To increase the visibility of KDE applications on Windows, the [KDE e.V.](https://ev.kde.org) has a Windows Store account to publish our applications there.

This is not the only way to get KDE application for the Windows operating system, you can e.g. directly grab installers or portable ZIP files from our [Binary Factory](https://binary-factory.kde.org).

There is at the moment no nice documentation how to submit some application to the store.

Hannah did show me how to do that during the submission of Kate and I did it later for Okular and Filelight.

Therefore I will now show the submission process for a new application in detail.

Each step has some screenshot of the web interface you need to use.

This requires that you have access to the KDE e.V. partner center on the Microsoft site shown below.

You can request access from our KDE system administrators.
To have this coordinated, please show up on [this Phabricator task](https://phabricator.kde.org/T9575).

### Creating a new application

You start your submission of new applications on the "Windows -> Overview" page of the [partner center](https://partner.microsoft.com/en-us/dashboard/windows/overview).

Just click there the "Create a new app" button.

<p align="center">
    <a href="/post/2019/2019-11-03-windows-store-submission-guide/images/windows-overview.png" target="_blank"><img width=500 src="/post/2019/2019-11-03-windows-store-submission-guide/images/windows-overview.png"></a>
</p>

### Choosing the application name

Next step is to select the application name.

For this guide, we will use Kile (and no, Kile is not yet submitted to the store, that will be finalized later, see [this task](https://phabricator.kde.org/T9584)).

<p align="center">
    <a href="/post/2019/2019-11-03-windows-store-submission-guide/images/application-name.png" target="_blank"><img width=500 src="/post/2019/2019-11-03-windows-store-submission-guide/images/application-name.png"></a>
</p>

### Starting a submission

Now we have a fresh generated application on the dashboard.

The next steps is now to create a first submission, for this you need to press the "Start your submission" button on the overview of the new application.

A submission is a bundle of both a new application version and the needed meta-data for the store, e.g. screenshots & descriptions.

<p align="center">
    <a href="/post/2019/2019-11-03-windows-store-submission-guide/images/start-submission.png" target="_blank"><img width=500 src="/post/2019/2019-11-03-windows-store-submission-guide/images/start-submission.png"></a>
</p>

### Fill out the different submission parts

You will end up on the overview page of the new submission.

The important parts to fill out are the top five

* Pricing and availability
* Properties
* Age ratings
* Packages
* Store listings

<p align="center">
    <a href="/post/2019/2019-11-03-windows-store-submission-guide/images/submission-overview.png" target="_blank"><img width=500 src="/post/2019/2019-11-03-windows-store-submission-guide/images/submission-overview.png"></a>
</p>

### Submission: Pricing and availability

For the pricing stuff, the defaults are sufficient for the most parts.

We want to have our stuff visible for public audience and we want to have it discoverable.

The default release schedule is fine, too, release as soon as possible and no end time for availability.

The only thing that needs here adjustments is the pricing, this should be set to "Free".

Afterwards, you can press the "Save draft" button at the bottom of the page.

<p align="center">
    <a href="/post/2019/2019-11-03-windows-store-submission-guide/images/pricing-availability.png" target="_blank"><img width=500 src="/post/2019/2019-11-03-windows-store-submission-guide/images/pricing-availability.png"></a>
</p>

### Submission: Properties

For the properties stuff, most important is the top part.

Very important: The category/subcategory for the store!
People will not be able to locate your stuff easily if you choose there some contra-intuitive stuff.

For example, Kile should be in something like "Productivity", like other tools of that kind.
If unsure, just browse a bit through the Microsoft store and take a look which kind of applications are where.

KDE provides a privacy policy website you can link to.

Below, insert the homepage of your application and the best location to get contact to you.

Most applications should have some contact/support/... page for this.

At the bottom you can configure some other things like system requirements.

For example Kile for sure wants some Keyboard or Mouse like input.

After you are done, scroll down and press the "Save" button.

<p align="center">
    <a href="/post/2019/2019-11-03-windows-store-submission-guide/images/properties.png" target="_blank"><img width=500 src="/post/2019/2019-11-03-windows-store-submission-guide/images/properties.png"></a>
</p>

### Submission: Age ratings

For age ratings, just follow the wizard.

<p align="center">
    <a href="/post/2019/2019-11-03-windows-store-submission-guide/images/age-ratings.png" target="_blank"><img width=500 src="/post/2019/2019-11-03-windows-store-submission-guide/images/age-ratings.png"></a>
</p>

This is more or less what you would expect. As the typical KDE application shall not contain sex, violence or other stuff of that kind, this should be straight forward.
Below my input for Kile.

<p align="center">
    <a href="/post/2019/2019-11-03-windows-store-submission-guide/images/age-ratings-filled.png" target="_blank"><img width=500 src="/post/2019/2019-11-03-windows-store-submission-guide/images/age-ratings-filled.png"></a>
</p>

Press "Save and generate" and be done.

The results will show up and if you see no issues, press "Continue" to finalize it.
As visible, Kile is applicable more or less to all ages.

<p align="center">
    <a href="/post/2019/2019-11-03-windows-store-submission-guide/images/age-ratings-results.png" target="_blank"><img width=500 src="/post/2019/2019-11-03-windows-store-submission-guide/images/age-ratings-results.png"></a>
</p>

### Submission: Packages

Now we get to submit the real "installer".

This is the "appxupload" package the Binary Factory will create for you.

For example for Kile, you can grab the latest release build at [Kile_Release_win64](https://binary-factory.kde.org/job/Kile_Release_win64/).

Before you upload stuff there, please really test this manually first!

Broken versions make a very bad impression.

I will only upload here some test version for the guide, later this will be replaced with a proper version before we submit Kile!

You can locally test on Windows the sideload.appx variant without going through the store, just click on it to install it locally.

For operating systems, at least select "Windows 10 Desktop".

<p align="center">
    <a href="/post/2019/2019-11-03-windows-store-submission-guide/images/packages.png" target="_blank"><img width=500 src="/post/2019/2019-11-03-windows-store-submission-guide/images/packages.png"></a>
</p>

After the package got successfully uploaded and validated, press "Save" to be done here.

### Submission: Store listings

Store listings are per language.

We start with the default, "English (United States)".

If you like, you can later add more languages there.

But keep in mind: You will need to update them manually on each new version, if you don't want to have the different languages with completely different store descriptions or screenshots.

To start with the English variant, click on the "English (United States)" link on the submission overview as seen below.

<p align="center">
    <a href="/post/2019/2019-11-03-windows-store-submission-guide/images/overview-store-listings.png" target="_blank"><img width=500 src="/post/2019/2019-11-03-windows-store-submission-guide/images/overview-store-listings.png"></a>
</p>

You will now end up on a page that allows to insert the usual stuff for an application store page.

Important here are for example:

* Description
* What's new in this version (important for later update submission)
* Product features (this is a list, add one feature in one field, you can add new fields with "Add more")
* Screenshots (the interface is a bit buggy, always take a look if e.g. the captions for the screenshots you added is still all right at the end of your editing)

A good location to grab this stuff from is the appdata.xml for your application.

For example for Kile you can grab this from the [KDE applications page for Kile](https://kde.org/applications/office/org.kde.kile).

After your are done, as always, press "Save" at the bottom of the page.

<p align="center">
    <a href="/post/2019/2019-11-03-windows-store-submission-guide/images/store-listings.png" target="_blank"><img width=500 src="/post/2019/2019-11-03-windows-store-submission-guide/images/store-listings.png"></a>
</p>

### Done => Press the button!

Now after you are done with this, you can just press the "Submit to the Store" button on the submission overview!

The application icon will show up properly in the web interface after the first submission is done, too.

For Kile, we still need to do final polishing before we can do this.

<p align="center">
    <a href="/post/2019/2019-11-03-windows-store-submission-guide/images/submit-to-the-store.png" target="_blank"><img width=500 src="/post/2019/2019-11-03-windows-store-submission-guide/images/submit-to-the-store.png"></a>
</p>

### Updates?

If you have your initial submitted application version in the store, updates are just new submissions.

Just head to the overview page of your application on the partner page and hit the "Update" button in the "Submissions" section.

The data from the last submission will be retained, you just need to upload a new installer + update the store listings page with "What's new in this version" and new screenshots if required.

### Join the Discussion

Feel free to join the discussion at the [KDE reddit](https://www.reddit.com/r/kde/comments/dr01th/windows_store_submission_guide/).
