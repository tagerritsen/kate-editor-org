---
title: Licensing of KDE Code
author: Dominik Haumann

date: 2015-07-26T12:57:27+00:00
url: /2015/07/26/licensing-of-kde-code/
categories:
  - Developers
tags:
  - planet

---
Akademy, the yearly KDE conference is alive and kicking. During the last days we were discussing again about potential KDE licensing issues (for instance code that is licensed under GPLv2, but not GPLv3). That&#8217;s why KDE is maintaining a relicense checker script, that **every KDE contributor** should enter herself/himself. I&#8217;ve blogged about it already <a href="/2015/02/21/add-yourself-to-the-relicense-checker-script/" target="_blank">in January 2015</a>, but it cannot hurt to repeat it from time to time: From the <a title="KDE Relicensing" href="https://techbase.kde.org/Projects/KDE_Relicensing" target="_blank">KDE relicensing page</a>:

> <p style="padding-left: 30px;">
>   A couple of KDE dependent projects or even libraries have moved or are going to move to GPLv3.
> </p>
> 
> <p style="padding-left: 30px;">
>   Unfortunately, GPL v3 is incompatible with GPL v2. This means that it is not possible to create a project linking GPL v2 and v3 code together. There is no problem for projects which are licensed GPLv2+ (version 2 or above).
> </p>
> 
> <p style="padding-left: 30px;">
>   A few parts of KDE are currently licensed as GPLv2 only. So far we have no reason to believe that this was something other than an oversight. However, we still need to validate with the individual copyright holders that a relicense to GPLv2+ or GPLv2+v3 is okay with them.
> </p>
> 
> <p style="padding-left: 30px;">
>   Therefore, in an effort we’re trying to identify the contributors that have contributed under the terms of GPLv2 and where the “+” part was not explicitly mentioned. If we know that all contributors agreed to a relicense, we can go ahead and flip the license of the individual source file.
> </p>

Short story: If you have not done so yet, **please add yourself to the kde <a title="KDE Relicense Checker" href="https://projects.kde.org/projects/kde/kdesdk/kde-dev-scripts/repository/revisions/master/entry/relicensecheck.pl" target="_blank">relicensecheck.pl</a> file!**

If you cannot add yourself right now, please consider sending a mail to <a href="mailto:kde-devel@kde.org" target="_blank">kde-devel@kde.org</a> publically stating that you&#8217;re ok with relicensing all your contributions hosted on the KDE services under either  GPLv23, LGPLv23, GPLv2+, LGPLv2+. Additionally, you can give the membership of the KDE e.V. the credentials to relicense your contributions in future (this makes a lot of sense if you cannot contribute to KDE anymore and are no longer around). Many of the KDE contributors did this already. Thanks! :-)

PS: The official way to give KDE the rights to change licenses would be by signing the <a href="https://ev.kde.org/rules/fla.php" target="_blank">KDE e.V.&#8217;s FLA</a>. Please note, that the KDE e.V. will not abuse this by immediately changing licenses you chose. Instead, this mostly about emergency cases (think of someone who suddenly passes away). But the script mentioned above is already a good start, since we can use it to quickly scan our code and identify problematic parts.

**Update:** With signing the FLA, you give the KDE e.V. several rights:

  * update KDE SC license
  * can protect KDE projects/code
  * can proect you (e.g. in copyright lawsuits)
  * make sure your code will live on even if you can&#8217;t contribute anymore

It is important to note, thought, that signing the FLA also includes that the KDE e.V. gives you back all your rights, so for you and your contributions, nothing changes. There is a BOF session about all this on Monday, so make sure you attend if you&#8217;re interested!