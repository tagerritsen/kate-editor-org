---
title: Windows Store Monthly Statistics
author: Christoph Cullmann
date: 2020-06-20T17:40:00+02:00
url: /post/2020/2020-06-20-windows-store-monthly-statistics/
---

[Kate](https://www.microsoft.com/store/apps/9NWMW7BB59HW) and [Okular](https://www.microsoft.com/store/apps/9N41MSQ1WNM8) 20.04 are now available in the store!

I hope this update solves some issues of the 19.12 versions available before.

Here are the number of acquisitions for the last 30 days (roughly equal to the number of installations, not mere downloads) for our applications:

* [Kate - Advanced Text Editor](https://www.microsoft.com/store/apps/9NWMW7BB59HW) - 4,465 acquisitions

* [Okular - More than a reader](https://www.microsoft.com/store/apps/9N41MSQ1WNM8) - 4,399 acquisitions

* [Filelight - Disk Usage Visualizer](https://www.microsoft.com/store/apps/9PFXCD722M2C) - 1,193 acquisitions

* [Kile - A user-friendly TeX/LaTeX editor](https://www.microsoft.com/store/apps/9PMBNG78PFK3) - 617 acquisitions

* [KStars - Astronomy Software](https://www.microsoft.com/store/apps/9PPRZ2QHLXTG) - 163 acquisitions

* [Elisa - Modern Music Player](https://www.microsoft.com/store/apps/9PB5MD7ZH8TL) - 138 acquisitions

A nice stream of new users for our software on the Windows platform.

If you want to help to bring more stuff KDE develops on Windows, we have some meta [Phabricator task](https://phabricator.kde.org/T9575) were you can show up and tell for which parts you want to do work on.

A guide how to submit stuff later can be found [on our blog](/post/2019/2019-11-03-windows-store-submission-guide/).

Thanks to all the people that help out with submissions & updates & fixes!

If you encounter issues on Windows and are a developer that wants to help out, all KDE projects really appreciate patches for Windows related issues.

Just contact the developer team of the corresponding application and help us to make the experience better on any operating system.

For completeness, overall acquisitions since the stuff is in the store:

* [Kate - Advanced Text Editor](https://www.microsoft.com/store/apps/9NWMW7BB59HW) - 46,824 acquisitions

* [Okular - More than a reader](https://www.microsoft.com/store/apps/9N41MSQ1WNM8) - 37,212 acquisitions

* [Filelight - Disk Usage Visualizer](https://www.microsoft.com/store/apps/9PFXCD722M2C) - 6,532 acquisitions

* [Kile - A user-friendly TeX/LaTeX editor](https://www.microsoft.com/store/apps/9PMBNG78PFK3) - 4,408 acquisitions

* [KStars - Astronomy Software](https://www.microsoft.com/store/apps/9PPRZ2QHLXTG) - 2,496 acquisitions

* [Elisa - Modern Music Player](https://www.microsoft.com/store/apps/9PB5MD7ZH8TL) - 1,450 acquisitions
