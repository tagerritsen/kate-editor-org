---
title: Kate LSP Status – July 21
author: Christoph Cullmann

date: 2019-07-21T13:18:00+00:00
excerpt: |
  The new LSP client by Mark Nauwelaerts keeps making nice progress.
  It will not be shipped with the KDE Applications 19.08 release, but in master it is now compiled &amp; installed per default.
  You only need to activate it on the plugin configuration pa...
url: /posts/kate-lsp-status-july-21/
enclosure:
  - |
    
    
    
syndication_source:
  - Posts on cullmann.io
syndication_source_uri:
  - https://cullmann.io/posts/
syndication_source_id:
  - https://cullmann.io/posts/index.xml
syndication_feed:
  - https://cullmann.io/posts/index.xml
syndication_feed_id:
  - "11"
syndication_permalink:
  - https://cullmann.io/posts/kate-lsp-status-july-21/
syndication_item_hash:
  - 218fb428e0343965ac2212d938813b43
categories:
  - Common

---
The new LSP client by Mark Nauwelaerts keeps making nice progress.

It will not be shipped with the KDE Applications 19.08 release, but in master it is now compiled & installed per default. You only need to activate it on the plugin configuration page in Kate&rsquo;s settings dialog to be able to use it.

For details how to build Kate master with it&rsquo;s plugins, please take a look at [this guide][1].

If you want to start to hack on the plugin, you find it in the kate.git, [addons/lspclient][2].

Feel welcome to show up on <kwrite-devel@kde.org> and help out! All development discussions regarding this plugin happen there.

If you are already familiar with Phabricator, post some patch directly at [KDE&rsquo;s Phabricator instance][3].

What is new this week?

The most thing are internal cleanups and minor improvements.

Feature wise, the hover implementation works now more like in other editors or IDEs, you get some nice tool tip after some delay:

<p align="center">
  <a href="https://cullmann.io/posts/kate-lsp-status-july-21/images/kate-hover.png" ><img width=500 src="https://cullmann.io/posts/kate-lsp-status-july-21/images/kate-hover.png"></a>
</p>

Never try to guess again what some _auto_ means ;=)

There is still a lot that can be improved, e.g. a filter for the symbols outline is in work:

  * [D22592 - port symbols view to model/view concept][4]

To be able to later out-source some parts of the generic LSP client code to a library, if there is demand, we will aim to make the plugin be licensed under the MIT license. This should make it easier for other projects to depend of our code, if wanted.

 [1]: /build-it/
 [2]: https://cgit.kde.org/kate.git/tree/addons/lspclient
 [3]: https://phabricator.kde.org/differential/
 [4]: https://phabricator.kde.org/D22592