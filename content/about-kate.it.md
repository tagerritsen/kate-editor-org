---
title: Funzionalità

author: Christoph Cullmann

date: 2010-07-09T08:40:19+00:00
---

## Funzionalità dell'applicazione

![Schermata che mostra la funzionalità di divisione della finestra di Kate e
l'estensione del terminale](/images/kate-window.png)

+ visualizza e modifica più documenti alla volta dividendo orizzontalmente o
verticalmente la finestra + tante estensioni: [Terminale
incorporato](https://konsole.kde.org), estensione per SQL, per la
compilazione, per GDB, Sostituisci nei file, e altro ancora + interfaccia
multi documento (MDI) + supporto per le sessioni

## Caratteristiche generali

![Schermata che mostra le funzioni di ricerca e sostituzione di
Kate](/images/kate-search-replace.png)

+ supporto per le codifiche (Unicode e molte altre) + supporto al rendering
bidirezionale del testo + supporto ai fine riga (Windows, Unix e Mac),
incluso il riconoscimento automatico + trasparenza di rete (apertura dei
file remoti) + estensibile tramite scripting

## Funzionalità di editor avanzate

![Schermata dell'area laterale di Kate con numeri di riga e
segnalibro](/images/kate-border.png)

+ sistema di segnalibri (supporta anche: punti di interruzione, ecc.) +
segni sulla barra di scorrimento + indicatori di righe modificate + numeri
delle righe + raggruppamento del codice

## Evidenziazione della sintassi

![Schermata della funzionalità di evidenziazione della sintassi di
Kate](/images/kate-syntax.png)

+ supporto all'evidenziazione per oltre 300 linguaggi + corrispondenza delle
parentesi + controllo ortografico intelligente durante la digitazione +
evidenziazione delle parole selezionate

## Funzionalità di programmazione

![Schermata delle funzionalità di programmazione di
Kate](/images/kate-programming.png)

+ rientro automatico regolabile con script + gestione intelligente
dell'aggiunta e rimozione dei commenti + completamento automatico con
suggerimento degli argomenti + modalità di inserimento Vi + modalità di
selezione a blocchi rettangolari

## Cerca e sostituisci

![Schermata della funzionalità di ricerca incrementale di
Kate](/images/kate-search.png)

+ ricerca incrementale, anche conosciuta come &#8220;cerca durante la
digitazione&#8221; + supporto per la ricerca e la sostituzione multi riga +
supporto alle espressioni regolari + ricerca e la sostituzione in più file
aperti o in più file su disco

## Copia di sicurezza e ripristino

![Schermata della funzionalità di recupero da un crash di
Kate](/images/kate-crash.png)

+ copie di sicurezza al salvataggio + file di swap per il recupero di dati
dopo un blocco del sistema + sistema di annulla / rifai
