---
title: Kate 16.04 on Windows (64bit)
author: Dominik Haumann

date: 2016-04-29T17:39:00+00:00
url: /2016/04/29/kate-16-04-on-windows-64bit/
categories:
  - Users
tags:
  - planet

---
### **<span style="color: #ff0000;">NOTE:</span> Please refer to [Get It](/get-it/) for up-to-date Windows versions**

It seems the [Kate Alpha Release in January][1] was well received by some users. So we are happy to announce that we are releasing an updated Windows installer for Kate (64bit) today. Essentially, this is the same version that is shipped with the <a href="https://www.kde.org/announcements/announce-applications-16.04.0.php" target="_blank">KDE Applications 16.04 release</a> under Linux.

  * <a href="http://download.kde.org/unstable/kate/" target="_blank">Kate-setup-16.04.1-64bit.exe</a> (64bit) installer, compiled on Windows 10
  * If you have git in your PATH, then the [Kate Projects plugin][2] will work as expected.
  * As mentioned in the <a href="/2016/01/28/kate-on-windows/" target="_blank">previous post</a>, you may want to also install <a href="https://dbus-windows-installer.googlecode.com/files/DBus-Windows-Installer-1.4.1-2.exe" target="_blank">D-Bus for Windows</a>, so that all documents opened use the same Kate instance.
  * You can compile Kate yourself with Visual Studio 2015 community edition by following the steps in <a href="https://quickgit.kde.org/?p=scratch%2Fsars%2Fkate-windows.git" target="_blank">kate-windows.git</a>. For me, a few tweaks were necessary, though: If some modules do not compile, just run the cmake build command again (and again). The &#8220;NMake Makefiles JOM&#8221; do not work for me, I had to use &#8220;Visual Studio 14 2015 Win64&#8221; generator. You can start the Visual Studio with \`devenv KateOnWindows.sln\`. If you have further questions, please contact us on the mailing list.

Screenshot of running Kate under Windows 10:

[<img class="aligncenter size-full wp-image-3731" src="/wp-content/uploads/2016/04/kate-on-win10.png" alt="Kate on Windows 10" width="1273" height="770" srcset="/wp-content/uploads/2016/04/kate-on-win10.png 1273w, /wp-content/uploads/2016/04/kate-on-win10-300x181.png 300w, /wp-content/uploads/2016/04/kate-on-win10-1024x619.png 1024w" sizes="(max-width: 1273px) 100vw, 1273px" />][3]If you want to contribute or have questions, please contact the Kate developers on <kwrite-devel@kde.org> (<a href="https://mail.kde.org/mailman/listinfo/kwrite-devel" target="_blank">subscribe</a>). Also, you can sometimes reach us via IRC on irc.freenode.net in the channel #kate.

Further, if you want to support Kate and the KDE project in general, it is very much appreciated if you <a href="https://www.kde.org/community/donations/" target="_blank">donate to the KDE e.V.</a>, KDE&#8217;s support organization &#8211; thank you! :-)

 [1]: /2016/01/28/kate-on-windows/
 [2]: /2014/10/12/autoloading-projects-plugin-kate-5/
 [3]: /wp-content/uploads/2016/04/kate-on-win10.png
