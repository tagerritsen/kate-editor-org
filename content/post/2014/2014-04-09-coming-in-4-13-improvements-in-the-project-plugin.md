---
title: 'Coming in 4.13: Improvements in the project plugin'
author: Alexander Neundorf

date: 2014-04-09T20:01:29+00:00
url: /2014/04/09/coming-in-4-13-improvements-in-the-project-plugin/
categories:
  - Common

---
Since version 4.10 Kate comes with a simple project plugin, as introduced <a href="/2012/11/02/using-the-projects-plugin-in-kate/" target="_blank">here</a> .  
The project plugin works by automatically reading a simple json file and providing the information found there to various parts and plugins in Kate.

## &#8220;Opening&#8221; a project

Projects are opened automatically by Kate. Whenever a file is opened, Kate goes the directories from that file upwards until it finds a file named .kateproject , which defines the project. This is a simple json file, which is intended to be written manually by the user.

The .kateproject file defines the name of the project, the set of files which belong to the project, and optionally commands for the <a href="/2014/03/16/coming-in-4-13-improvements-in-the-build-plugin/" target="_blank">build plugin</a> .

In 4.13, &#8220;out-of-source&#8221; project files are now also supported (actually already in 4.12).  What does that mean ? You can create a .kateproject file in some directory, but it will refer to a different directory as root of the project. This is useful if you have multiple build trees for one source tree, and then need different build commands for each build tree.

Creating such a .kateproject file is easy, simply add a top-level &#8220;directory&#8221; entry:

<pre>{
    "name": "MyProject",
    "directory": "/home/alex/src/myproject",
    "files": [ { "filters": [ "*.cpp", "*.h"] } ]
}</pre>

So if you create this file e.g. in /home/alex/src/myproject-build/.kateproject, once the project is opened, the files below /home/alex/src/myproject/ will belong to the project &#8220;MyProject&#8221;. Again, to &#8220;open&#8221; this project, open _any_ file in the same directory as the .kateproject file or any of its subdirectories in Kate. Kate will again automatically find the .kateproject file and load it.

## Support for the improved build plugin

In 4.13 the build plugin has seen <a href="/2014/03/16/coming-in-4-13-improvements-in-the-build-plugin/" target="_blank">several improvements</a>, the main one being that it is now possible to define an arbitrary number of targets, instead of being limited to 3. This is fully supported by the project plugin. Also the &#8220;old&#8221; format is still fully supported, and the .kateproject files can even contain both the old and the new format, so it works with the build plugin in version before 4.13 and also after.

Below there is a simple example for a hello-world project, which defines 4 targets for the build plugin: build all, clean, install and building just &#8220;hello&#8221;:

<pre>{
    "name": "Hello",
    "files": [ { "filters": [ "*.cpp", "*.h"] } ],

    "build": {
        "directory": "/home/alex/src/tests/hello/build",
        "targets":[
             {"name":"all", "build_cmd":"make -j4 all"}
            ,{"name":"clean", "build_cmd":"make -j4 clean"}
            ,{"name":"hello", "build_cmd":"make -j4 hello"}
            ,{"name":"install", "build_cmd":"make install"}
        ], 
        "default_target": "all",
        "clean_target": "clean"
    }
}</pre>

So,  for each target, a &#8220;name&#8221; and a &#8220;build_cmd&#8221; is defined, and that&#8217;s it. One of the targets can be chosen to be the default target (which can be assigned a dedicated shortcut in the build plugin), and one can be chosen to be the &#8220;clean&#8221; target (again, which can be assigned a dedicated shortcut in the build plugin).

The screenshot below shows what you get when opening this project in Kate:

<figure id="attachment_3274" aria-describedby="caption-attachment-3274" style="width: 1016px" class="wp-caption aligncenter">[<img class=" wp-image-3274" alt="kate-project-plugin-manual1" src="/wp-content/uploads/2014/03/kate-project-plugin-manual1.png" width="1016" height="795" srcset="/wp-content/uploads/2014/03/kate-project-plugin-manual1.png 1016w, /wp-content/uploads/2014/03/kate-project-plugin-manual1-300x234.png 300w" sizes="(max-width: 1016px) 100vw, 1016px" />][1]<figcaption id="caption-attachment-3274" class="wp-caption-text">Build plugin showing 4 targets from a .kateproject file</figcaption></figure>

I mentioned above that both the &#8220;old&#8221; and the new build plugin can be supported within one .kateproject file. To do that, simply put both target definitions in the file, they don&#8217;t interfer:

<pre>{
    "name": "Hello",
    "files": [ { "filters": [ "*.cpp", "*.h"] } ],

    "build": {
        "directory": "/home/alex/src/tests/hello/build",
        "targets":[
             {"name":"all", "build_cmd":"make -j4 all"}
            ,{"name":"clean", "build_cmd":"make -j4 clean"}
            ,{"name":"hello", "build_cmd":"make -j4 hello"}
            ,{"name":"install", "build_cmd":"make install"}
        ], 
        "default_target": "all",
        "clean_target": "clean",

        "build": "make -j4 all",
        "clean": "make -j4 clean",
        "quick": "make -j4 install",
    }
}</pre>

Here, additionally to the 4 custom build targets, the three hardcoded targets &#8220;build&#8221;, &#8220;clean&#8221; and &#8220;quick&#8221; for the &#8220;old&#8221; build plugin are defined. When this project is opened in Kate 4.13 or newer, these three old entries are ignored, and only the four new entries are used. When this project is opened in Kat 4.12 or earlier, the four new targets are ignored and only the old ones are used.

In the case that an &#8220;old&#8221; .kateproject file is opened, which contains only the old entries, these are used, and the three entries are used to create three targets, as shown in the screenshot below:

<figure id="attachment_3294" aria-describedby="caption-attachment-3294" style="width: 989px" class="wp-caption aligncenter">[<img class="size-full wp-image-3294" alt="Build plugin showing 3 targets from an &quot;old&quot; .kateproject file" src="/wp-content/uploads/2014/03/kate-project-plugin-old-project-file.png" width="989" height="760" srcset="/wp-content/uploads/2014/03/kate-project-plugin-old-project-file.png 989w, /wp-content/uploads/2014/03/kate-project-plugin-old-project-file-300x230.png 300w" sizes="(max-width: 989px) 100vw, 989px" />][2]<figcaption id="caption-attachment-3294" class="wp-caption-text">Build plugin showing 3 targets from an &#8220;old&#8221; .kateproject file</figcaption></figure>

&nbsp;

## Using the project plugin with CMake-based projects

Until now, the only way to create .kateproject files was to write them manually. If you are using Kate with C/C++ projects which are built using CMake, there are more news for you. <a href="http://www.cmake.org/pipermail/cmake/2014-March/057283.html" target="_blank">CMake 3.0.0</a> will be released soon, and among others, it will contain a generator for, guess what: project files for the Kate project plugin!  With that, run CMake, select &#8220;Kate &#8211; Unix Makefiles&#8221; as generator, and there you go, everything set up ready to use for you, including all targets of the project available in the build plugin.

Below is a screenshot showing running cmake-gui on CMake itself:

<figure id="attachment_3300" aria-describedby="caption-attachment-3300" style="width: 812px" class="wp-caption aligncenter">[<img class="size-full wp-image-3300" alt="Running cmake-gui on the CMake sources, showing the available generators" src="/wp-content/uploads/2014/03/kate-cmake-cmake.png" width="812" height="583" srcset="/wp-content/uploads/2014/03/kate-cmake-cmake.png 812w, /wp-content/uploads/2014/03/kate-cmake-cmake-300x215.png 300w" sizes="(max-width: 812px) 100vw, 812px" />][3]<figcaption id="caption-attachment-3300" class="wp-caption-text">Running cmake-gui on the CMake sources, showing the available generators</figcaption></figure>

As you can see, <a href="http://martine.github.io/ninja/manual.html" target="_blank">ninja</a> is also supported.

Personally I still prefer Makefiles, especially for use with Kate projects. When using the Makefile generator, you get build targets for compiling every individual source file into an object file. This can save a lot of time when working on some source file and trying to get it to compile. Instead of starting to build everything, which involves dependency checking, and linking afterwards, you can simply just compile that one file (via the quick target select dialog of the updated [build plugin][4]), and if it failed, simply build the previous target again (there&#8217;s a shortcut for that) until it compiles, and then switch back to building everything (by building the default target).

In the screenshot below you can see Kate having loaded the project for CMake itself, listing the whole bunch of source files on the left, a long list of available build targets in the lower part, and the select-target-dialog on top, filtered already and the target for compiling cmMakefile.cxx is selected.

<figure id="attachment_3303" aria-describedby="caption-attachment-3303" style="width: 945px" class="wp-caption aligncenter">[<img class="size-full wp-image-3303" alt="Kate showing a full project for CMake itself" src="/wp-content/uploads/2014/03/kate-project-plugin-cmake-select-targets.png" width="945" height="872" srcset="/wp-content/uploads/2014/03/kate-project-plugin-cmake-select-targets.png 945w, /wp-content/uploads/2014/03/kate-project-plugin-cmake-select-targets-300x276.png 300w" sizes="(max-width: 945px) 100vw, 945px" />][5]<figcaption id="caption-attachment-3303" class="wp-caption-text">Kate showing a full project for CMake itself</figcaption></figure>

&nbsp;

After trying to compile the file, Kate shows you which errors occurred, and using a dedicated shortcut (I set it to F9) it jumps to the line in the code:

<figure id="attachment_3276" aria-describedby="caption-attachment-3276" style="width: 939px" class="wp-caption aligncenter">[<img class="size-full wp-image-3276" alt="Build plugin: jump to error works" src="/wp-content/uploads/2014/03/kate-project-plugin-cmake-compile-file-error.png" width="939" height="721" srcset="/wp-content/uploads/2014/03/kate-project-plugin-cmake-compile-file-error.png 939w, /wp-content/uploads/2014/03/kate-project-plugin-cmake-compile-file-error-300x230.png 300w" sizes="(max-width: 939px) 100vw, 939px" />][6]<figcaption id="caption-attachment-3276" class="wp-caption-text">Build plugin: jump to error works</figcaption></figure>

As can be seen, there is the parsed error, the status tells you that there were errors when building the target &#8220;cmMakefile.cxx.o&#8221;, and if you want to try again, there&#8217;s a &#8220;Build again&#8221; button right there.

Now, how do you actually open CMake-generated projects in Kate? The .kateproject file is generated in the build tree, and usually you never have to open any files from the build tree in Kate.  But to open the project in Kate, you have to open any, at least one, file from the build tree (this will trigger searching the .kateproject file, which will point the project plugin to the source tree). To help with this, CMake additionally generates a file &#8220;ProjectName@buildDirectory.kateproject&#8221; in the top level build dir, right next to the generated .kateproject file. Open this file, and Kate loads the project.

&nbsp;

<figure id="attachment_3279" aria-describedby="caption-attachment-3279" style="width: 939px" class="wp-caption aligncenter">[<img class="size-full wp-image-3279" alt="Loading a project via opening the &quot;dummy&quot; ProjectName@BuildDir.kateproject file" src="/wp-content/uploads/2014/03/kate-project-plugin-cmake-project-file.png" width="939" height="721" srcset="/wp-content/uploads/2014/03/kate-project-plugin-cmake-project-file.png 939w, /wp-content/uploads/2014/03/kate-project-plugin-cmake-project-file-300x230.png 300w" sizes="(max-width: 939px) 100vw, 939px" />][7]<figcaption id="caption-attachment-3279" class="wp-caption-text">Loading a project via opening the &#8220;dummy&#8221; ProjectName@BuildDir.kateproject file</figcaption></figure>

This has been done in the screenshot above,  and already you&#8217;re ready to go !

## Documentation

Last but not least, starting with 4.13, you can find documentation for the project json file in <PREFIX>/share/apps/apps/kate/plugins/project/kateproject.example.

&nbsp;

So that&#8217;s it for now, I hope you find the new stuff useful. :-)

 [1]: /wp-content/uploads/2014/03/kate-project-plugin-manual1.png
 [2]: /wp-content/uploads/2014/03/kate-project-plugin-old-project-file.png
 [3]: /wp-content/uploads/2014/03/kate-cmake-cmake.png
 [4]: /2014/03/16/coming-in-4-13-improvements-in-the-build-plugin/
 [5]: /wp-content/uploads/2014/03/kate-project-plugin-cmake-select-targets.png
 [6]: /wp-content/uploads/2014/03/kate-project-plugin-cmake-compile-file-error.png
 [7]: /wp-content/uploads/2014/03/kate-project-plugin-cmake-project-file.png