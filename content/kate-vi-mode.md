---
title: VI Mode
author: Erlend Hamberg
date: 2010-08-30T18:09:51+00:00
---

## Introduction

<img class="alignright" title="vim" src="/wp-content/uploads/2010/08/vim.png" alt="Vim Logo" width="96" height="96" />
Kate&#8217;s VI mode is a project to bring Vim-like, modal editing to the [Kate text editor][1] and by extension to other KDE programs who share the same editor component. The project started as a Google Summer of Code project in 2008 – where all the basic functionality was written. I have continued to maintain and further develop this code and the number of missing features from Vim are slowly decreasing. Most Vim users will already be productive in Kate&#8217;s VI mode. A list of what&#8217;s missing is found at the bottom of the page.

This page is meant to be an updated overview of this work.

<div style="border: 1px solid black; margin: 0px auto -1px; padding: 1em; width: 75%; background-color: #eeeeee; text-align: left;">
  To enable the VI input mode, go to<br /> <strong>Settings → Configure Kate… → Editing → VI Input Mode</strong>.<br /> It can also be toggled with the “VI Input Mode” setting in the “Edit” menu. (The default shortcut key is Meta+Ctrl+V – where Meta usually is the Windows key).
</div>

## Goals

The goal of the VI mode is _not_ to be a complete replacement for Vim and support _all_ Vim&#8217;s features. Its aim is to make the “Vim way” of text editing – and the Vim habits learned – available for programs using the Kate text editor as their internal editor. These programs include

  1. The Kate Text editor
  2. KWrite – KDE&#8217;s simple text editor
  3. KDevelop – An advanced IDE for many programming languages
  4. Kile – A LaTeX editor

The VI mode aims integrate nicely with the programs and deviate from Vim&#8217;s behaviour where it makes sense. For example, **:w** will open a save dialogue in Kate&#8217;s VI mode.

## Incompatibilities with Vim

There are only a few features of Kate&#8217;s VI mode which are incompatible with Vim (not counting things missing). They are listed below together with the respective reasons.

  1. **Kate:** U and ctrl+r is redo
    **Vim:** ctrl+r is normal redo, U is used to undo all latest changes on one line
    The reason for having U act as redo in Kate&#8217;s VI mode is that the shortcut ctrl+r by default is taken by Kate&#8217;s replace function (search and replace). By default, the VI mode won&#8217;t override Kate&#8217;s shortcuts (this can be configured in Settings → Configure Kate… → Editing → Vi Input Mode), therefore a redo-action needs to be available as a “regular” key press, too. Besides, the behaviour of the U command in Vim doesn&#8217;t map well to Kate&#8217;s internal undo system, so it would be non-trivial to support anyway.
  2. **Kate:** :print shows the &#8216;print&#8217; dialogue
    **Vim:** :print prints the lines of the given range like its grandfather ed
    Commands like :print are available not only in the VI mode but for users using “regular” Kate, too – I have therefore chosen to let the :print command open the print dialogue – following the principle of least surprise instead of mimicking Vim&#8217;s behaviour.
  3. **Kate:** &#8216;Y&#8217; yanks to end of line.
    **Vim:** &#8216;Y&#8217; yanks whole line, just like &#8216;yy&#8217;.
    VI&#8217;s behaviour for the &#8216;Y&#8217; command is in practice a bug; For both change and delete commands, &#8216;cc&#8217;/&#8217;dd&#8217; will do its action on the current line and &#8216;C&#8217;/&#8217;D&#8217; will work from the cursor column to the end of the line. However, both &#8216;yy&#8217; and &#8216;Y&#8217; yanks the current line.In Kate&#8217;s VI Mode &#8216;Y&#8217; will yank to the end of the line. This is described as “more logical” [in the Vim documentation][2].
  4. **Kate:** :map alters the selected lines of the document using the provided Javascript expression.
    **Vim:** :map adds the provided mapping to Normal and Visual modes.
    The &#8220;map&#8221; command was already reserved by Kate; in 4.12+, you can use a combination of :nmap and :vmap to replace it.

## Supported Commands

<h3 style="margin-top: 1em;">
  Supported normal/visual mode commands
</h3>

<table style="margin-left: 3em; background-color: #efefef;" border="0" width="390">
  <tr>
    <td>
      a
    </td>

    <td>
      Enter Insert Mode and append
    </td>
  </tr>

  <tr>
    <td>
      A
    </td>

    <td>
      Enter Insert Mode and append to EOL
    </td>
  </tr>

  <tr>
    <td>
      i
    </td>

    <td>
      Enter Insert Mode
    </td>
  </tr>

  <tr>
    <td>
      I
    </td>

    <td>
      Insert before first non-blank char in line
    </td>
  </tr>

  <tr>
    <td>
      v
    </td>

    <td>
      Enter Visual Mode
    </td>
  </tr>

  <tr>
    <td>
      V
    </td>

    <td>
      Enter Visual Line Mode
    </td>
  </tr>

  <tr>
    <td>
      <c-v>
    </td>

    <td>
      Enter Visual Block Mode
    </td>
  </tr>

  <tr>
    <td>
      gv
    </td>

    <td>
      Re-select&nbsp;Visual
    </td>
  </tr>

  <tr>
    <td>
      o
    </td>

    <td>
      Open new line under
    </td>
  </tr>

  <tr>
    <td>
      O
    </td>

    <td>
      Open new line over
    </td>
  </tr>

  <tr>
    <td>
      J
    </td>

    <td>
      Join lines
    </td>
  </tr>

  <tr>
    <td>
      c
    </td>

    <td>
      Change
    </td>
  </tr>

  <tr>
    <td>
      C
    </td>

    <td>
      Change to EOL
    </td>
  </tr>

  <tr>
    <td>
      cc
    </td>

    <td>
      Change line
    </td>
  </tr>

  <tr>
    <td>
      s
    </td>

    <td>
      Substitute char
    </td>
  </tr>

  <tr>
    <td>
      S
    </td>

    <td>
      Substitute line
    </td>
  </tr>

  <tr>
    <td>
      dd
    </td>

    <td>
      Delete line
    </td>
  </tr>

  <tr>
    <td>
      d
    </td>

    <td>
      Delete
    </td>
  </tr>

  <tr>
    <td>
      D
    </td>

    <td>
      Delete to EOL
    </td>
  </tr>

  <tr>
    <td>
      x
    </td>

    <td>
      Delete char
    </td>
  </tr>

  <tr>
    <td>
      X
    </td>

    <td>
      Delete char backward
    </td>
  </tr>

  <tr>
    <td>
      gu
    </td>

    <td>
      Make lowercase
    </td>
  </tr>

  <tr>
    <td>
      guu
    </td>

    <td>
      Make lowercase line
    </td>
  </tr>

  <tr>
    <td>
      gU
    </td>

    <td>
      Make uppercase
    </td>
  </tr>

  <tr>
    <td>
      gUU
    </td>

    <td>
      Make uppercase line
    </td>
  </tr>

  <tr>
    <td>
      y
    </td>

    <td>
      Yank
    </td>
  </tr>

  <tr>
    <td>
      yy
    </td>

    <td>
      Yank line
    </td>
  </tr>

  <tr>
    <td>
      Y
    </td>

    <td>
      Yank to EOL
    </td>
  </tr>

  <tr>
    <td>
      p
    </td>

    <td>
      Paste
    </td>
  </tr>

  <tr>
    <td>
      P
    </td>

    <td>
      Paste before
    </td>
  </tr>

  <tr>
    <td>
      r.
    </td>

    <td>
      Replace character
    </td>
  </tr>

  <tr>
    <td>
      R
    </td>

    <td>
      Enter replace mode
    </td>
  </tr>

  <tr>
    <td>
      :
    </td>

    <td>
      Switch to command line
    </td>
  </tr>

  <tr>
    <td>
      /
    </td>

    <td>
      Search
    </td>
  </tr>

  <tr>
    <td>
      u
    </td>

    <td>
      Undo
    </td>
  </tr>

  <tr>
    <td>
      <c-r>
    </td>

    <td>
      Redo
    </td>
  </tr>

  <tr>
    <td>
      U
    </td>

    <td>
      Redo
    </td>
  </tr>

  <tr>
    <td>
      m.
    </td>

    <td>
      Set mark
    </td>
  </tr>

  <tr>
    <td>
      >>
    </td>

    <td>
      Indent line
    </td>
  </tr>

  <tr>
    <td>
      <<
    </td>

    <td>
      Unindent line
    </td>
  </tr>

  <tr>
    <td>
      >
    </td>

    <td>
      Indent lines
    </td>
  </tr>

  <tr>
    <td>
      <
    </td>

    <td>
      Unindent lines
    </td>
  </tr>

  <tr>
    <td>
      <c-f>
    </td>

    <td>
      Scroll page down
    </td>
  </tr>

  <tr>
    <td>
      <pagedown>
    </td>

    <td>
      Scroll page down
    </td>
  </tr>

  <tr>
    <td>
      <c-b>
    </td>

    <td>
      Scroll page up
    </td>
  </tr>

  <tr>
    <td>
      <pageup>
    </td>

    <td>
      Scroll page up
    </td>
  </tr>

  <tr>
    <td>
      <c-u>
    </td>

    <td>
      Scroll half page up
    </td>
  </tr>

  <tr>
    <td>
      <c-d>
    </td>

    <td>
      Scroll half page down
    </td>
  </tr>

  <tr>
    <td>
      zz
    </td>

    <td>
      Centre view on cursor
    </td>
  </tr>

  <tr>
    <td>
      ga
    </td>

    <td>
      Print character code
    </td>
  </tr>

  <tr>
    <td>
      .
    </td>

    <td>
      Repeat last change
    </td>
  </tr>

  <tr>
    <td>
      ==
    </td>

    <td>
      Align line
    </td>
  </tr>

  <tr>
    <td>
      =
    </td>

    <td>
      Align lines
    </td>
  </tr>

  <tr>
    <td>
      ~
    </td>

    <td>
      Change case
    </td>
  </tr>

  <tr>
    <td>
      <c-a>
    </td>

    <td>
      Add to number
    </td>
  </tr>

  <tr>
    <td>
      <c-x>
    </td>

    <td>
      Subtract from number
    </td>
  </tr>

  <tr>
    <td>
      <c-o>
    </td>

    <td>
      Go to prev jump
    </td>
  </tr>

  <tr>
    <td>
      <c-i>
    </td>

    <td>
      Go to next jump
    </td>
  </tr>

  <tr>
    <td>
      <c-w>h
    </td>

    <td>
      Switch to left view
    </td>
  </tr>

  <tr>
    <td>
      <c-w><c-h>
    </td>

    <td>
      Switch to left view
    </td>
  </tr>

  <tr>
    <td>
      <c-w><left>
    </td>

    <td>
      Switch to left view
    </td>
  </tr>

  <tr>
    <td>
      <c-w>j
    </td>

    <td>
      Switch to down view
    </td>
  </tr>

  <tr>
    <td>
      <c-w><c-j>
    </td>

    <td>
      Switch to down view
    </td>
  </tr>

  <tr>
    <td>
      <c-w><down>
    </td>

    <td>
      Switch to down view
    </td>
  </tr>

  <tr>
    <td>
      <c-w>k
    </td>

    <td>
      Switch to up view
    </td>
  </tr>

  <tr>
    <td>
      <c-w><c-k>
    </td>

    <td>
      Switch to up view
    </td>
  </tr>

  <tr>
    <td>
      <c-w><up>
    </td>

    <td>
      Switch to up view
    </td>
  </tr>

  <tr>
    <td>
      <c-w>l
    </td>

    <td>
      Switch to right view
    </td>
  </tr>

  <tr>
    <td>
      <c-w><c-l>
    </td>

    <td>
      Switch to right view
    </td>
  </tr>

  <tr>
    <td>
      <c-w><right>
    </td>

    <td>
      Switch to right view
    </td>
  </tr>

  <tr>
    <td>
      <c-w>w
    </td>

    <td>
      Switch to next view
    </td>
  </tr>

  <tr>
    <td>
      <c-w><c-w>
    </td>

    <td>
      Switch to next view
    </td>
  </tr>

  <tr>
    <td>
      <c-w>s
    </td>

    <td>
      Split horizontally
    </td>
  </tr>

  <tr>
    <td>
      <c-w>S
    </td>

    <td>
      Split horizontally
    </td>
  </tr>

  <tr>
    <td>
      <c-w><c-s>
    </td>

    <td>
      Split horizontally
    </td>
  </tr>

  <tr>
    <td>
      <c-w>v
    </td>

    <td>
      Split vertically
    </td>
  </tr>

  <tr>
    <td>
      <c-w><c-v>
    </td>

    <td>
      Split vertically
    </td>
  </tr>

  <tr>
    <td>
      gt
    </td>

    <td>
      Switch to next tab
    </td>
  </tr>

  <tr>
    <td>
      gT
    </td>

    <td>
      Switch to prev tab
    </td>
  </tr>

  <tr>
    <td>
      gqq
    </td>

    <td>
      Format line
    </td>
  </tr>

  <tr>
    <td>
      gq
    </td>

    <td>
      Format lines
    </td>
  </tr>

  <tr>
    <td>
      q. / q
    </td>

    <td>
      Begin/ finish recording macro using the named macro register.
    </td>
  </tr>
</table>

<h3 style="margin-top: 1em;">
  Supported motions
</h3>

<table style="margin-left: 3em; background-color: #eee;" border="0">
  <tr>
    <td>
      h
    </td>

    <td>
      Left
    </td>
  </tr>

  <tr>
    <td>
      <left>
    </td>

    <td>
      Left
    </td>
  </tr>

  <tr>
    <td>
      <backspace>
    </td>

    <td>
      Left
    </td>
  </tr>

  <tr>
    <td>
      j
    </td>

    <td>
      Down
    </td>
  </tr>

  <tr>
    <td>
      <down>
    </td>

    <td>
      Down
    </td>
  </tr>

  <tr>
    <td>
      <enter>
    </td>

    <td>
      Down to first non blank
    </td>
  </tr>

  <tr>
    <td>
      k
    </td>

    <td>
      Up
    </td>
  </tr>

  <tr>
    <td>
      <up>
    </td>

    <td>
      Up
    </td>
  </tr>

  <tr>
    <td>
      &#8211;
    </td>

    <td>
      Up to first non blank
    </td>
  </tr>

  <tr>
    <td>
      l
    </td>

    <td>
      Right
    </td>
  </tr>

  <tr>
    <td>
      <right>
    </td>

    <td>
      Right
    </td>
  </tr>

  <tr>
    <td>
      <space>
    </td>

    <td>
      Right
    </td>
  </tr>

  <tr>
    <td>
      $
    </td>

    <td>
      To EOL
    </td>
  </tr>

  <tr>
    <td>
      <end>
    </td>

    <td>
      To EOL
    </td>
  </tr>

  <tr>
    <td>
    </td>

    <td>
      To 0 column
    </td>
  </tr>

  <tr>
    <td>
      <home>
    </td>

    <td>
      To 0 column
    </td>
  </tr>

  <tr>
    <td>
      ^
    </td>

    <td>
      To first character of line
    </td>
  </tr>

  <tr>
    <td>
      f.
    </td>

    <td>
      Find char
    </td>
  </tr>

  <tr>
    <td>
      F.
    </td>

    <td>
      Find char backward
    </td>
  </tr>

  <tr>
    <td>
      t.
    </td>

    <td>
      To char
    </td>
  </tr>

  <tr>
    <td>
      T.
    </td>

    <td>
      To char backward
    </td>
  </tr>

  <tr>
    <td>
      ;
    </td>

    <td>
      Repeat last t. or f. command
    </td>
  </tr>

  <tr>
    <td>
      ,
    </td>

    <td>
      Repeat last t. or f. command
    </td>
  </tr>

  <tr>
    <td>
      n
    </td>

    <td>
      Find next
    </td>
  </tr>

  <tr>
    <td>
      N
    </td>

    <td>
      Find prev
    </td>
  </tr>

  <tr>
    <td>
      gg
    </td>

    <td>
      To first line
    </td>
  </tr>

  <tr>
    <td>
      G
    </td>

    <td>
      To last line
    </td>
  </tr>

  <tr>
    <td>
      w
    </td>

    <td>
      Word forward
    </td>
  </tr>

  <tr>
    <td>
      W
    </td>

    <td>
      WORD forward
    </td>
  </tr>

  <tr>
    <td>
      b
    </td>

    <td>
      Word backward
    </td>
  </tr>

  <tr>
    <td>
      B
    </td>

    <td>
      WORD backward
    </td>
  </tr>

  <tr>
    <td>
      e
    </td>

    <td>
      To end of word
    </td>
  </tr>

  <tr>
    <td>
      E
    </td>

    <td>
      To end of WORD
    </td>
  </tr>

  <tr>
    <td>
      ge
    </td>

    <td>
      To end of prev word
    </td>
  </tr>

  <tr>
    <td>
      gE
    </td>

    <td>
      To end of prev WORD
    </td>
  </tr>

  <tr>
    <td>
      |
    </td>

    <td>
      To screen column
    </td>
  </tr>

  <tr>
    <td>
      %
    </td>

    <td>
      To matching item
    </td>
  </tr>

  <tr>
    <td>
      `[a-zA-Z><]
    </td>

    <td>
      To mark
    </td>
  </tr>

  <tr>
    <td>
      &#8216;[a-zA-Z><]
    </td>

    <td>
      To mark line
    </td>
  </tr>

  <tr>
    <td>
      [[
    </td>

    <td>
      To previous brace block start
    </td>
  </tr>

  <tr>
    <td>
      ]]
    </td>

    <td>
      To next brace block start
    </td>
  </tr>

  <tr>
    <td>
      []
    </td>

    <td>
      To previous brace block end
    </td>
  </tr>

  <tr>
    <td>
      ][
    </td>

    <td>
      To next brace block end
    </td>
  </tr>

  <tr>
    <td>
      *
    </td>

    <td>
      To next occurrence of word under cursor
    </td>
  </tr>

  <tr>
    <td>
      #
    </td>

    <td>
      To prev occurrence of word under cursor
    </td>
  </tr>

  <tr>
    <td>
      H
    </td>

    <td>
      To first line of window
    </td>
  </tr>

  <tr>
    <td>
      M
    </td>

    <td>
      To middle line of window
    </td>
  </tr>

  <tr>
    <td>
      L
    </td>

    <td>
      To last line of window
    </td>
  </tr>

  <tr>
    <td>
      gj
    </td>

    <td>
      To next visual line
    </td>
  </tr>

  <tr>
    <td>
      gk
    </td>

    <td>
      To prev visual line
    </td>
  </tr>
</table>

<h3 style="margin-top: 1em;">
  Supported text objects
</h3>

<table style="margin-left: 3em; background-color: #eee;" border="0">
  <tr>
    <td>
      iw
    </td>

    <td>
      Inner word
    </td>
  </tr>

  <tr>
    <td>
      aw
    </td>

    <td>
      A word
    </td>
  </tr>

  <tr>
    <td>
      iW
    </td>

    <td>
      Inner WORD
    </td>
  </tr>

  <tr>
    <td>
      aW
    </td>

    <td>
      A WORD
    </td>
  </tr>

  <tr>
    <td>
      i&#8221;
    </td>

    <td>
      Inner double quote
    </td>
  </tr>

  <tr>
    <td>
      a&#8221;
    </td>

    <td>
      A double quote
    </td>
  </tr>

  <tr>
    <td>
      i&#8217;
    </td>

    <td>
      Inner single quote
    </td>
  </tr>

  <tr>
    <td>
      a&#8217;
    </td>

    <td>
      A single quote
    </td>
  </tr>

  <tr>
    <td>
      i`
    </td>

    <td>
      Inner back quote
    </td>
  </tr>

  <tr>
    <td>
      a`
    </td>

    <td>
      A back quote
    </td>
  </tr>

  <tr>
    <td>
      ib
    </td>

    <td>
      Inner paren
    </td>
  </tr>

  <tr>
    <td>
      i)
    </td>

    <td>
      Inner paren
    </td>
  </tr>

  <tr>
    <td>
      i(
    </td>

    <td>
      Inner paren
    </td>
  </tr>

  <tr>
    <td>
      ab
    </td>

    <td>
      A paren
    </td>
  </tr>

  <tr>
    <td>
      a)
    </td>

    <td>
      A paren
    </td>
  </tr>

  <tr>
    <td>
      a(
    </td>

    <td>
      A paren
    </td>
  </tr>

  <tr>
    <td>
      iB
    </td>

    <td>
      Inner curly bracket
    </td>
  </tr>

  <tr>
    <td>
      o}
    </td>

    <td>
      Inner curly bracket
    </td>
  </tr>

  <tr>
    <td>
      i{
    </td>

    <td>
      Inner curly bracket
    </td>
  </tr>

  <tr>
    <td>
      aB
    </td>

    <td>
      A curly bracket
    </td>
  </tr>

  <tr>
    <td>
      a}
    </td>

    <td>
      A curly bracket
    </td>
  </tr>

  <tr>
    <td>
      a{
    </td>

    <td>
      A curly bracket
    </td>
  </tr>

  <tr>
    <td>
      i<
    </td>

    <td>
      Inner inequality sign
    </td>
  </tr>

  <tr>
    <td>
      i>
    </td>

    <td>
      Inner inequality sign
    </td>
  </tr>

  <tr>
    <td>
      a<
    </td>

    <td>
      A inequality sign
    </td>
  </tr>

  <tr>
    <td>
      a>
    </td>

    <td>
      A inequality sign
    </td>
  </tr>

  <tr>
    <td>
      i[
    </td>

    <td>
      Inner bracket
    </td>
  </tr>

  <tr>
    <td>
      I]
    </td>

    <td>
      Inner bracket
    </td>
  </tr>

  <tr>
    <td>
      a[
    </td>

    <td>
      A bracket
    </td>
  </tr>

  <tr>
    <td>
      a]
    </td>

    <td>
      A bracket
    </td>
  </tr>

  <tr>
    <td>
      i,
    </td>

    <td>
      Inner comma
    </td>
  </tr>

  <tr>
    <td>
      a,
    </td>

    <td>
      A comma
    </td>
  </tr>
</table>

<h3 style="margin-top: 1em;">
  Supported insert mode commands
</h3>

<table style="margin-left: 3em; background-color: #eee;" border="0">
  <tr>
    <td>
      <c-d>
    </td>

    <td>
      Unindent
    </td>
  </tr>

  <tr>
    <td>
      <c-t>
    </td>

    <td>
      Indent
    </td>
  </tr>

  <tr>
    <td>
      <c-e>
    </td>

    <td>
      Insert from below
    </td>
  </tr>

  <tr>
    <td>
      <c-y>
    </td>

    <td>
      Insert from above
    </td>
  </tr>

  <tr>
    <td>
      <c-w>
    </td>

    <td>
      Delete word
    </td>
  </tr>

  <tr>
    <td>
      <c-r>.
    </td>

    <td>
      Insert content of register
    </td>
  </tr>

  <tr>
    <td>
      <c-o>
    </td>

    <td>
      Switch to normal mode for one command
    </td>
  </tr>

  <tr>
    <td>
      <c-a>
    </td>

    <td>
      Increase number under cursor
    </td>
  </tr>

  <tr>
    <td>
      <c-x>
    </td>

    <td>
      Decrease number under cursor
    </td>
  </tr>
</table>

<a name="comma"></a>

<h2 style="margin-top: 1em;">
  The <em>Comma</em> Text Object
</h2>

This is something that I have been missing in Vim. The _comma_ text object makes it easy to modify parameter lists in C-like languages and other comma separated lists. It is basically the area between two commas or between a comma and a bracket. In the line shown in the illustration to the right, the three ranges this text object can span are highlighted in red.<figure id="attachment_992" aria-describedby="caption-attachment-992" style="width: 383px" class="wp-caption alignnone">

[<img class="size-full wp-image-992 " title="comma_to" src="/wp-content/uploads/2012/06/comma_to.png" alt="" width="383" height="87" />][3]<figcaption id="caption-attachment-992" class="wp-caption-text">Comma text object ranges. If the cursor is over, say, “arg2”, pressing c i , (“change inner comma”) would delete “double arg2” and place the cursor between the two commas in insert mode. A very convenient way to change a function&#8217;s parameters.</figcaption></figure>

<a name="emulatedcommandbar"></a>

## Emulated Vim Command Bar

Kate 4.11 introduced a hidden config option that make &#8220;/&#8221;, &#8220;?&#8221; and &#8220;:&#8221; bring up a new search/ command in place of the usual Kate Find/ Replace / Command-line bar. The bar is intended to intended to replicate many of the features of Vim&#8217;s command bar, and also to fix many of the issues with Kate Vim mode&#8217;s interaction with Kate&#8217;s Find/ Replace bar (interactive replace not working; incremental search not positioning the cursor correctly; not usable in mappings/ macros; etc).

The following shortcuts are provided by the emulated command bar; as with Vim, these can be remapped with cmap, cnoremap, etc:

<table style="margin-left: 3em; background-color: #efefef;" border="0" width="390">
  <tr>
    <td>
      <c-r>.
    </td>

    <td>
      insert contents of register.
    </td>
  </tr>

  <tr>
    <td>
      <c-r><c-w>
    </td>

    <td>
      Insert word under the (document) cursor.
    </td>
  </tr>

  <tr>
    <td>
      <c-p>
    </td>

    <td>
      Invoke context-specific completion (see below)/ move back/ up in the completion list.
    </td>
  </tr>

  <tr>
    <td>
      <c-p>
    </td>

    <td>
      Move forward/ down in the completion list.
    </td>
  </tr>

  <tr>
    <td>
      <c-space>
    </td>

    <td>
      <strong>Kate Vim Extension.</strong> Auto-complete word from document.
    </td>
  </tr>

  <tr>
    <td>
      <c-d>
    </td>

    <td>
      <strong>Kate Vim Extension.</strong> In a sed-replace expression (i.e. one of the form s/find/replace/[g][c][i]), clear the &#8220;find&#8221; term and place the cursor there.
    </td>
  </tr>

  <tr>
    <td>
      <c-f>
    </td>

    <td>
      <strong>Kate Vim Extension.</strong> In a sed-replace expression (i.e. one of the form s/find/replace/[g][c][i]), clear the &#8220;replace&#8221; term and place the cursor there.
    </td>
  </tr>

  <tr>
    <td>
      <c-g>.
    </td>

    <td>
      <strong>Kate Vim Extension.</strong> As with <c-r>., insert the content of the named register, but escape it in such a way that when used with a search, we search for the <em>literal</em> content of the register; not the content of the register interpreted as a regex.
    </td>
  </tr>
</table>

The &#8220;context-specific completion&#8221; is decided as follows:

  * In a search bar (&#8220;/&#8221; or &#8220;?&#8221;), auto-complete from search history (which includes searches initiated via &#8220;*&#8221; and &#8220;#&#8221;; searches done in sed-replace expressions; etc.)
  * In an empty command bar (&#8220;:&#8221;), auto-complete from command history (NB: auto-completion of command names is invoked automatically when you begin typing).
  * In a command-bar containing a sed-replace expression (e.g. &#8220;&#8216;<,&#8217;>s/find/replace/gc&#8221;), if the cursor is positioned over &#8220;find&#8221;, auto-complete from the &#8220;search&#8221; history; if over the &#8220;replace&#8221;, auto-complete from the history of &#8220;replace&#8221; terms.

When executing a sed-replace expression in the command bar with the &#8220;c&#8221; flag (e.g. s/find/replace/gc), a Vim-style interactive search/replace expression will be initiated.

Some example usages of the emulated command bar, with GIF animations, are given in this [blog][4]. In 4.11, the emulated command bar can be enabled by setting the hidden config option &#8220;Vi Input Mode Emulate Command Bar&#8221; to &#8220;true&#8221;. in your katerc/ kwriterc/ kdeveloprc.

## Missing Features

As stated earlier, the goal of Kate&#8217;s VI Mode is not to support 100% of Vim&#8217;s features, however, there are some features which are sorely missed

  * Visual block mode – especially the ability to prepend/append text to the visual block selection.
  * Having ex commands available in other programs than the Kate application.
  * The search code needs improvement and the * and # commands should just be regular searches.

If you miss other features or want to help on the ones mentioned above, feel free to contact me or send patches! :-)

<div>
  <h2>
    Change Log
  </h2>

  <ul>
    <li>
      <strong>2010-05-16:</strong><br /> Initial version. Collected the information from blog entries and README files to make a single source of current information.
    </li>
    <li>
      <strong>2010-05-17:<br /> </strong>Ctrl<strong>+</strong>A and Ctrl+X added (increment/decrement number under cursor).
    </li>
    <li>
      <strong>2010-08-30:<br /> </strong>Moved page to kate-editor.org.
    </li>
    <li>
      <strong>2010-09-10:<br /> </strong>Fixed the text on the comma text object and made some formatting fixe<strong>s </strong>
    </li>
  </ul>
</div>

 [1]: https://www.kate-editor.org
 [2]: http://vimdoc.sourceforge.net/htmldoc/change.html#Y "Vimdoc: Y"
 [3]: /wp-content/uploads/2012/06/comma_to.png
 [4]: /2013/09/06/kate-vim-progress/
