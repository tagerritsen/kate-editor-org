# kate-editor.org

This repository contains the full kate-editor.org website.

# Instantiate the page

You can create the static page by running

./update.sh

This will pull the git repo and run hugo with the right settings.
A checked in version of hugo is used that shall run on any CentOS >= 7 or compatible Linux x86-64 distribution.

# Live preview

You can start a local hugo powered web server via

./server.sh

The command will print the URL to use for local previewing.

# Update the syntax-highlighting framework update sites

Run the update regeneration script, that will pull in the necessary stuff from anongit.kde.org

./update-syntax.pl

Afterwards, check the files that were added to your local git clone and commit/push if that looks ok.
(all files updated shall be in static/syntax/... + the auto-generated content/syntax.md page)

This needs to be done after each frameworks release.
