---
title: 'Idea: Code Completion for JavaScript'
author: Dominik Haumann

date: 2013-02-27T20:17:54+00:00
url: /2013/02/27/idea-code-completion-for-javascript/
pw_single_layout:
  - "1"
categories:
  - Developers
tags:
  - planet

---
In 2012, Kate had a GSoC project with the aim of adding a [Scripting IDE plugin][1]. The idea was to make it easy to write [Kate Part scripts][2] (command line scripts, indenters), that should include rudimentary code completion and a gui to manage available scripts. As detailed in the blog post, a rudimentary list that showed all available scripts is available. However, due to changes in Kate Part&#8217;s scripting framework (e.g. changes in the folder structure, require() function to load libraries), the plugin in its current form is not applicable anymore. Still, the idea of having a better JavaScript support is valid.

So this blog post is about some ideas how we can add possibly really cool JavaScript support to Kate as a plugin. This plugin should provide the following features:

  1. provide code completion
  2. support for loading additional modules (jQuery, Node.js, Prototype, Kate Scripting API, &#8230;)
  3. specialized support for Kate Part scripting
  4. outline in terms of a function and prototype browser

### JavaScript Code Completion

Providing code completion to JavaScript requires a parser so we have an abstract syntax tree (AST) of the current file. This parser needs to be error-tolerant, since during the process of programming, the source code often violates the syntax. Searching the Web, relatively few information about such a parser exists. But one tiny parser shines: <a title="Esprima" href="http://esprima.org/" target="_blank">Esprima</a> (<a title="Esprima source code" href="https://github.com/ariya/esprima" target="_blank">source code</a>, <a title="Infos about Esprima" href="http://ariya.ofilabs.com/2012/07/behind-esprima.html" target="_blank">more info</a>), written by KDE&#8217;s &#8220;very own&#8221; Ariya :-)

Esprima is a JavaScript parser written in JavaScript. It is (to some degree) error-tolerant and returns an <a title="Esprima Parse Demo" href="http://esprima.org/demo/parse.html" target="_blank">AST as shown in the parse demo</a>. The AST is returned in simple <a title="JSON" href="http://www.json.org/" target="_blank">JSON</a> syntax, meaning it can be easily parsed with either the <a title="JSON in JavaScript" href="http://www.json.org/js.html" target="_blank">JavaScript JSON functions</a> or <a title="JSON in Qt5" href="http://qt-project.org/wiki/Qt-5Features#080262b7904bf7cd6d839d85e0727cd1" target="_blank">JSON in Qt5</a> (<a title="QJSON (Qt4)" href="http://qjson.sourceforge.net/" target="_blank">JSON for Qt4</a>). Along the error-tolerant mode, the AST returned by Esprima also contains comments and line/column information, if desired.

Interestingly, the idea of using Esprima for code completion <a title="Code Completion with Esprima" href="https://code.google.com/p/esprima/issues/detail?id=130" target="_blank">arose about a year ago</a>, and meanwhile there are several projects that successfully use the Esprima AST: <a title="Esprima in Eclipse Orion" href="http://ariya.ofilabs.com/2012/07/code-editing-with-autocompletion-in-eclipse-orion.html" target="_blank">Eclipse Orion</a>, a web-based IDE, uses Esprima for <a title="Orion Code Completion" href="http://contraptionsforprogramming.blogspot.de/2012/02/better-javascript-content-assist-in.html" target="_blank">code completion</a> and for the <a title="Outline View based on Esprima" href="https://github.com/aclement/esprima-outline" target="_blank">outline view (sources)</a>. The source code for the code completion is available in the <a title="Orion IDE git" href="http://wiki.eclipse.org/Orion/Getting_the_source" target="_blank">orion client git module</a>: <a title="Esprima code" href="http://git.eclipse.org/c/orion/org.eclipse.orion.client.git/tree/bundles/org.eclipse.orion.client.ui/web/esprima" target="_blank">lib</a>, <a title="Esprima Code Completion (Content Assist)" href="http://git.eclipse.org/c/orion/org.eclipse.orion.client.git/tree/bundles/org.eclipse.orion.client.ui/web/plugins/esprima" target="_blank">code completion (content assist)</a>, <a title="Esprima unit test for code completion" href="http://git.eclipse.org/c/orion/org.eclipse.orion.client.git/tree/bundles/org.eclipse.orion.client.ui/web/js-tests/esprima" target="_blank">unit tests</a>.

### So What Next?

As it seems the code is all there, the next step would be to start a small standalone project and write a proof of concept to check whether this approach is feasible. I&#8217;d suggest to use Qt&#8217;s JavaScript interpreter (<a title="QtScript in Qt4" href="http://qt-project.org/doc/qt-4.8/qtscript.html" target="_blank">QtScript</a> in Qt4, <a title="QJS in Qt5" href="http://qt-project.org/doc/qt-5.0/qtqml/qtqml-module.html" target="_blank">QJS in Qt5</a>). In the long run, the goal would be to have code completion and possibly an outline view. If this works, more functionality like support for Kate Scripting could be added. So there is a lot to play around with.

If you are interested, just start working on it. It might be a good GSoC project (although we&#8217;d require a quite skilled developer here) :-P Thoughts? Comments?

 [1]: /2012/08/29/1982/ "Scripting IDE"
 [2]: http://docs.kde.org/stable/en/kde-baseapps/kate/advanced-editing-tools-scripting.html "Scripting Kate Part"